GIT_BRANCH_NAME := $(shell git rev-parse --abbrev-ref HEAD | awk -F '/' '{print $$NF}')

#  k8s 主链浏览器镜像打包，hub-dev.cnbn.org.cn只能内网访问
docker-main-build:
	docker build -t chainmaker1.tencentcloudcr.com/opennet/explorer_front:v${GIT_BRANCH_NAME} -t hub-dev.cnbn.org.cn/opennet/explorer_front:v${GIT_BRANCH_NAME} -f ./build-main.Dockerfile .

#  k8s 主链浏览器镜像上传
docker-main-push:
	docker push hub-dev.cnbn.org.cn/opennet/explorer_front:v${GIT_BRANCH_NAME}
	docker push chainmaker1.tencentcloudcr.com/opennet/explorer_front:v${GIT_BRANCH_NAME}

#  k8s 主链浏览器镜像生产打包
docker-product-build:
	docker build -t 112.64.115.246:19981/opennet/explorer_front:v1.0.1 -f ./build-main.Dockerfile .

#  k8s 主链浏览器镜像生产上传
docker-product-push:
	docker push 112.64.115.246:19981/opennet/explorer_front:v1.0.1

#  k8s 主链浏览器镜像打包
docker-test-build:
	docker build -t hub-dev.cnbn.org.cn/opennet/explorer-front-testnet:latest -f ./build-test.Dockerfile .

#  k8s 主链浏览器镜像上传
docker-test-push:
	docker push hub-dev.cnbn.org.cn/opennet/explorer-front-testnet:latest

k8s_create_configmap:
	kubectl create cm config-front --from-file=./k8s/config/default.conf --dry-run=client -o yaml > ./k8s/config-front.yaml
	kubectl apply -f ./k8s/config-front.yaml

k8s_apply_formal:
	kubectl delete -f ./k8s/deploy-explorer-front.yaml
	kubectl apply -f ./k8s/deploy-explorer-front.yaml

k8s_create_configmap_test:
	kubectl create cm config-front --from-file=./k8s-test/config/default.conf --dry-run=client -o yaml > ./k8s-test/config-front.yaml
	kubectl apply -f ./k8s-test/config-front.yaml

k8s_apply_testnet:
	kubectl delete -f ./k8s-test/deploy-explorer-front.yaml
	kubectl apply -f ./k8s-test/deploy-explorer-front.yaml
