import { ParamWithChainId } from '.';
import { PagingWithChainId } from './common';

export interface GetTxListParam extends PagingWithChainId {
  /**
   * 交易id值
   */
  TxId?: string;
  // 合约方法
  ContractMethod?: string;
  /**
   * 合约名
   */
  ContractName?: string;
  /**
   * 发送用户名
   */
  Senders?: string;
  /**
   * 区块hash值
   */
  BlockHash?: string;
  /**
   * 开始时间
   */
  StartTime?: number;
  /**
   * 结束时间
   */
  EndTime?: number;
  /**
   * 交易发送者地址
   */
  UserAddrs?: string;
}

export interface Tx extends GetLatestTxListItem {
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 交易发送组织
   */
  SenderOrgId: string;
  /**
   * 交易状态（0:成功，1失败）
   */
  TxStatus: number;
  /**
   * 区块hash
   */
  BlockHash: string;

  // 合约方法
  ContractMethod: string;
}

export interface TxInfo extends Tx {
  /**
   * 交易hash
   */
  TxHash: string;
  /**
   * 详情内字段是否可见，1为不可见
   */
  ShowStatus: 0 | 1;
  /**
   * 交易类型
   */
  TxType: string;
  /**
   * 合约版本
   */
  ContractVersion: string;
  /**
   * 交易merkle哈希
   */
  TxRootHash: string;
  /**
   * 合约执行结果码
   */
  ContractResultCode: number;
  /**
   * 合约执行结果
   */
  ContractResult: string;
  /**
   * 交易信息
   */
  RwSetHash: string;
  /**
   * 合约调用方法
   */
  ContractMethod: string;
  /**
   * 参数
   */
  ContractParameters: string;
  /**
   * 参数列表
   */
  ContractParametersList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
    /**
     * 解码的值
     */
    decodeValue?: {
      index: number;
      type: string;
      value: string;
    }[];
  }[];
  /**
   * 读集列表
   */
  ContractRead: string;
  ContractReadList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
  }[];
  /**
   * 合约信息
   */
  ContractMessage?: string;
  /**
   * GAS消耗量
   */
  GasUsed?: string;
  /**
   * 写集列表
   */
  ContractWrite: string;
  ContractWriteList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;
  }[];
  /**
   * 代付者
   */
  Payer: string;
  Event: string;
  EventList?: {
    /**
     * key属性
     */
    key: string;
    /**
     * 值
     */
    value: string;

    /**
     * 合约名
     */
    contractName: string;
  }[];
  /** 合约类型 */
  RuntimeType: string;

  ContractType: string;
}

export type GetLatestTxListParam = ParamWithChainId;

export interface GetLatestTxListItem {
  /**
   * 交易Id
   */
  TxId: string;
  /**
   * 交易发送者
   */
  Sender: string;
  /**
   * 合约名
   */
  ContractName: string;
  /**
   * 合约地址
   */
  ContractAddr?: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 交易发送者地址
   */
  UserAddr: string;
  /**
   * 交易发送者bns
   */
  UserAddrBns: string;
}


export interface GetUserTxListParam extends PagingWithChainId {

  /**
   * 交易发送者地址
   */
  UserAddrs?: string;
}

export interface GetContractTxListParam extends PagingWithChainId {
  ContractAddr?: string;

  /**
   * 交易发送者地址
   */
  UserAddrs?: string;

  ConnectorUserAddr?: string;


  // 合约方法
  ContractMethod?: string;

  ParamKey?: string;

  ConnectorParamKey?: string;
  /**
   * 开始时间
   */
  StartTime?: number;
  /**
   * 结束时间
   */
  EndTime?: number;

  ConnectorTime?: string;
}


export interface GetQueryTxListParam extends PagingWithChainId {
  Operator: 'or' | 'and';

  ContractName?: string;

  ContractAddr?: string;

  ConnectorUserAddr?: string;

  // 合约方法
  ContractMethod?: string;

  TxId?: string;

  StartTime?: number;

  EndTime?: number;

  TxStatus?: string;

  UserAddr?: string;
}


export interface GetBlockTxListParam extends PagingWithChainId {
  BlockHash?: string;
}
