import { PagingWithChainId } from './common';

export interface GetTransferListParam extends PagingWithChainId {
  /**
   * 合约名称
   */
  ContractName: string;
  TokenId?: string;
}

export interface TransferItem {
  Id: number;
  /**
   * 上链时间
   */
  BlockTime: number;
  /**
   * 交易id
   */
  TxId: string;
  /**
   * 合约方法
   */
  ContractMethod: string;
  /**
   * 来源
   */
  From: string;
  /**
   * 转向的地址
   */
  To: string;
  /**
   * TokenId
   */
  TokenId: string;
  /**
   * 交易状态
   */
  Status: string;
}
