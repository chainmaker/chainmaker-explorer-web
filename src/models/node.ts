import { PagingWithChainId } from './common';

export interface GetNodeListParam extends PagingWithChainId {
  /**
   * 节点名称
   */
  NodeName: string;
  /**
   * 组织id
   */
  OrgId: string;
  /**
   * 节点id
   */
  NodeId: string;
}

export interface NodeItem {
  /**
   * 节点逻辑id
   */
  Id: number;
  /**
   * 节点Id
   */
  NodeId: string;
  /**
   * 节点名称
   */
  NodeName: string;
  /**
   * 节点地址
   */
  NodeAddress: string;
  /**
   * 用户角色
   */
  Role: string;
  /**
   * 组织名
   */
  OrgId: string;
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * @deprecated
   * 创建时间，该时间并不正确
   */
  Timestamp: number;
}
