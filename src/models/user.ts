import { PagingWithChainId } from '.';

export interface GetUserListParam extends PagingWithChainId {
  /**
   * 用户id
   */
  UserIds: string;
  /**
   * 组织id
   */
  OrgId?: string;
}

export interface UserItem {
  /**
   * 用户id
   */
  UserId: string;
  /**
   * 用户地址
   */
  UserAddr: string;
  /**
   * 用户角色
   */
  Role: string;
  /**
   * 组织id
   */
  OrgId: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}
