import { ParamWithChainId, PagingWithChainId } from './common';

export type GetDecimalParam = ParamWithChainId;
export type GetTxNumByTimeParam = ParamWithChainId;
export interface GetTxNumByTimeItem {
  /**
   * 交易数量
   */
  TxNum: number;
  /**
   * 时间戳
   */
  Timestamp: number;
}
export interface ChainInfo {
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 最近24小时区块数量
   */
  RecentBlockHeight: number;
  /**
   * 交易总数
   */
  TxCount: number;
  /**
   * 最近24小时交易
   */
  RecentTxNum: number;
  /**
   * 合约数量
   */
  ContractCount: number;
  /**
   * 最近24小时新增合约
   */
  RecentContractNum: number;
  /**
   * 组织数量
   */
  OrgCount: number;
  /**
   * 节点数量
   */
  RunningNode: number;
  /**
   * 用户数量
   */
  UserCount: number;
  /**
   * 最近24小时新增合约
   */
  RecentUserNum: number;
}

export interface SearchParam {
  /**
   * 链id
   */
  ChainId?: string;
  /**
   * 查询输入值
   */
  Value: string;
  /**
   * 查询类型
   */
  Type?: string;
}

export interface SearchInfo {
  /**
   * 链id
   */
  ChainId: string;
  /**
   * 返回数据类型 -1:未知结果；0：区块详情；1：交易详情；2：合约详情； 3:账户详情
   */
  Type: number;
  /**
   * 查询详情逻辑ID
   */
  Id: number;
  /**
   * 不同类型的不同标识，区块：区块hash   交易：交易id   合约：合约名
   */
  Data: string;
  // 合约类型（token类型：CMDFA, ERC20; NFT类型：CMNFA, ERC721）
  ContractType: string;
}

export interface CrossSearchParam {
  /**
   * 链id
   */
  ChainId?: string;
  /**
   * 查询输入值
   */
  Value: string;
}

export interface CrossSearchInfo {
  /**
   * 返回数据类型 -1:未知结果；0：区块详情；1：交易详情；2：合约详情； 3:账户详情 
   */
  Type: number;
  /**
   * 不同类型的不同标识，区块：区块hash   交易：交易id   合约：合约名
   */
  Data: string;
}

export interface CrossOverviewDataInfo {
  // 总区块高度
  TotalBlockHeight: number;
  // 最短时间（单位s）
  ShortestTime: number;
  // 最长时间（单位s）
  LongestTime: number;
  // 平均时间（单位s）
  AverageTime: number;
  // 子链数量
  SubChainNum: number;
  // 跨链交易数
  TxNum?: number;
}

// 获取最新跨链交易列表
export interface CrossLatestTxListItem {
  // 跨链ID
  CrossId: string;
  // 发起链名称
  FromChainName: string;
  // 发起链id
  FromChainId: string;
  // 是否是主链
  FromIsMainChain: boolean;
  // 目标链名称
  ToChainName: string,
  // 目标链id
  ToChainId: string,
  // 是否是主链
  ToIsMainChain: boolean,
  // 跨链状态（0:成功，1:失败）
  Status: number,
  // 跨链发起时间
  Timestamp : number
}

export interface CrossLatestSubChainListItem {
  // 子链ID
  SubChainId: string;
  // 子链名称
  SubChainName: string;
  // 子链高度
  BlockHeight: number;
  // 跨链交易数
  CrossTxNum: number;
  // 跨链合约数
  CrossContractNum: number;
  // 子链状态（0:正常，1:异常）
  Status: number;
  // 同步时间
  Timestamp: number
}

export interface CrossSubChainListParam extends PagingWithChainId {
  // 子链id
  SubChainId?: string;
  // 子链名称
  SubChainName?: string;
}

export interface CrossSubChainListItem extends CrossLatestSubChainListItem {
  // 浏览器地址
  ExplorerUrl: string;
}

export interface CrossSubChainDetailInfo {
  // 子链ID
  SubChainId: string;
  // 子链名称
  SubChainName: string;
  // 子链高度
  BlockHeight: number;
  // 区块链架构（1 长安链，2 fabric，3 bcos， 4eth，5+ 扩展）
  ChainType: number;
  // 跨链交易数
  CrossTxNum: number;
  // 跨链合约数
  CrossContractNum: number;
  // 子链状态（0:正常，1:异常）
  Status: number;
  // 跨链网关ID
  GatewayId: string;
  // 跨链网关名称
  GatewayName: string;
  // 跨链网关地址
  GatewayAddr: string;
}

export interface CrossTxChainInfo {
  // 链名称
  ChainName: string;
  // 链ID
  ChainId: string;
  // 合约名称
  ContractName: string;
  // 交易ID
  TxId: string;
  // 交易状态（0:成功，1:失败）
  TxStatus: string;
  // 交易跳转地址
  TxUrl: string;
  // 是否是主链（0:子链，1:主链）
  IsMainChain: boolean;
  // gas消耗
  Gas: string;
}

export interface CrossTxDetailInfo {
  // 跨链ID
  CrossId: string;
  // 跨链状态（0:新建，1：待执行，2:待提交，3:确认结束，4:回滚结束）
  Status: number;
  // 跨链完成时间（单位s）
  CrossDuration: number;
  // 跨链发起时间
  Timestamp: number;
  // 合约名称
  ContractName: string;
  // 合约方法
  ContractMethod: string;
  // 合约入参
  Parameter: string;
  // 合约执行结果json
  ContractResult: string;
  // 跨链方向
  CrossDirection: {
    // 来源链名称
    FromChain: string;
    // 目标链名称
    ToChain: string;
  };
  // 来源链数据
  FromChainInfo: CrossTxChainInfo;
  // 目标链数据
  ToChainInfo: CrossTxChainInfo;
}

export interface SubChainCrossChainListItem {
  // 链名称
  ChainName: string;
  // 链ID
  ChainId: string;
  // 排名
  Ranking: number;
  // 跨链交易数
  TxNum: number;
}

export interface GetCrossTxListParam extends PagingWithChainId {
  // 跨链id
  CrossId?: string;
  // 子链id
  SubChainId?: string;
  // 发起链名称
  FromChainName?: string;
  // 接收链名称
  ToChainName?: string;
  // 开始时间
  StartTime?: number;
  // 结束时间
  EndTime?: number;
}

export interface CrossTxListItem {
  // 跨链ID
  CrossId: string;
  // 来源链名称
  FromChainName: string;
  // 来源链ID
  FromChainId: string;
  // 是否是主链
  FromIsMainChain: boolean;
  // 目标链名称
  ToChainName: string;
  // 目标链ID
  ToChainId: string;
  // 是否是主链
  ToIsMainChain: boolean;
  // 跨链状态（0:进行中，1:成功，2:失败）
  Status: string;
  // 跨链发起时间
  Timestamp: number;
}