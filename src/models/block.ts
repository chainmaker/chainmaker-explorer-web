import { PagingWithChainId, ParamWithChainId } from '.';

export interface GetBlockListParam extends PagingWithChainId {
  /**
   * 区块hash值
   */
  BlockKey?: string;
  /**
   * 节点id
   */
  NodeId?:string;
}

export interface BlockDetail {
  /**
   * 区块高度
   */
  BlockHeight: number;
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 交易数量
   */
  TxNum: number;
  /**
   * 时间戳
   */
  Timestamp: number;
}
export interface BlockItem extends BlockDetail {
  /**
   * 提案节点
   */
  ProposalNodeId: string;
}

export interface GetBlockDetailParam extends ParamWithChainId {
  /**
   * 区块hash
   */
  BlockHash: string | undefined;
}
export interface BlockInfo {
  /**
   * 区块hash
   */
  BlockHash: string;
  /**
   * 前一个区块hash
   */
  PreBlockHash: string;
  /**
   * 提案节点
   */
  ProposalNodeId: string;
  /**
   * 交易根hash
   */
  TxRootHash: string;
  /**
   * 读写集hash
   */
  RwSetHash: string;
  // /**
  //  * 交易数量
  //  */
  // TxCount: number;
  /**
   * 区块hash
   */
  BlockHeight: number;
  /**
   * 提案组织id
   */
  OrgId: string;
  /**
   * Dag特征摘要
   */
  Dag: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}

export type GetLatestBlockListParam = ParamWithChainId;

export interface GetLatestBlockListItem extends BlockDetail {
  /**
   * 交易发送者
   */
  ProposalNodeId: string;
}
