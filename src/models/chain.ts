import { ChainStatusVales } from '@src/utils/enums';
import { Paging, ParamWithChainId } from './common';

export type CancelSubscribeParam = ParamWithChainId;
export type DeleteSubscribeParam = ParamWithChainId;

export interface ChainListParam extends Paging {
  /**
   * 根据ChainId搜索
   */
  ChainId?: string;
}

// 链列表中的链
export interface ChainItem {
  /**
   * 账户模式 permissionedwithcert（证书模式cert链）  public（公钥模式pk链）
   */
  AuthType: string;
  /**
   * 链逻辑ID
   */
  Id: number;
  /**
   * 链ID
   */
  ChainId: string;
  /**
   * 链版本
   */
  ChainVersion: string;
  /**
   * 共识算法
   */
  Consensus: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 状态
   */
  Status: ChainStatusVales;

  NodeAddrList: string[];

  /**
   * JSON序列化后值
   */
  ChainConfig: string;
}

/**
 * 监听链的参数
 */
export interface Subscribe {
  /**
   * 链id
   */
  ChainId?: string;
  /**
   * 账户模式
   */
  AuthType: string;
  /**
   * 组织Id
   */
  OrgId?: string;
  /**
   * 用户证书
   */
  UserSignCrt?: string;
  /**
   * 用户key
   */
  UserSignKey: string;

  UserEncKey?: string;

  UserEncCrt?: string;

  /**
   * 密码算法
   *   0: '国密',
   *   1: '非国密',
   */
  HashType?: string | number;

  /**
   * 0：单证书模式，1：双证书模式
   */
  TlsMode: 0 | 1;
  /**
   * 节点列表
   */
  NodeList: {
    /**
     * 地址
     */
    Addr: string;

    /**
     * node节点的CA证书
     */
    OrgCA?: string;
    /**
     * tls连接使用的域名
     */
    TLSHostName?: string;
  }[];
  /**
   * 是否开启 tls
   */
  Tls?: boolean;
  UserTlsCrt?:string;
  UserTlsKey?:string;
}
