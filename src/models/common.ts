export interface Paging {
  /**
   * 分页偏移量
   */
  Offset?: number;
  /**
   * 当前查询的数量，默认为每页显示数量
   */
  Limit: number;
}

export interface PagingWithChainId extends Paging {
  /**
   * 链id
   */
  ChainId: string | undefined;
}

export interface ParamWithChainId {
  /**
   * 链id
   */
  ChainId: string | undefined;
}

export interface UiPaging {
  /**
   * 分页偏移量
   */
  pageIndex: number;
  /**
   * 当前查询的数量，默认为每页显示数量
   */
  pageSize: number;
}
export type ValueOf<T> = T[keyof T];

export interface ParamWithSubChainId extends ParamWithChainId {
  /**
   * 子链id
   */
  SubChainId: string | undefined;
}

export interface ParamWithCrossId extends ParamWithChainId {
  /**
   * 跨链id
   */
  CrossId: string;
}