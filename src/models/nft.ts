import { PagingWithChainId, ValueOf } from './common';
import { ContractStatus } from '@src/utils/enums';

export interface GetNFTContractListParam extends PagingWithChainId {
  // 链ID
  ChainId: string;
  // 合约名称/合约地址
  ContractKey?: string;
}

export interface NFTContractItem {
  // 合约名称
  ContractName: string;
  // 合约地址
  ContractAddr: string;
  // 合约类型
  ContractType: string;
  // 累计交易数
  TxNum: number;
  // 发行总量
  TotalSupply: string;
  // 持有人数
  HolderCount: number;
  // 创建时间
  Timestamp: number;
  // tokeId
  TokenId: string;
}

export interface GetNFTPositionListParam extends PagingWithChainId {
  // 合约地址
  ContractAddr: string;
  // 持仓地址
  OwnerAddr?: string;
}

export interface NFTPositionItem {
  // 持仓地址类型：0:用户，1:合约地址
  AddrType: number;
  // 合约地址
  ContractAddr: string;
  // 合约地址
  ContractName: string;
  // 持仓地址
  OwnerAddr: string;
  // 持有排名
  HoldRank: number;
  // 持有数量
  Amount: string;
  // 持有比例
  HoldRatio: string;
}

export interface GetNFTContractDetailParam {
  // 链ID
  ChainId: string;
  // 合约地址
  ContractAddr: string;
}

export interface GetAccountDetailParam {
  // 链ID
  ChainId: string;
  // 账户地址
  Address?: string;
  // BNS地址
  BNS?: string;
}

export interface NFTContractDetail {
  // 合约名称
  ContractName: string;
  // 合约地址
  ContractAddr: string;
  // 合约状态码(0: 正常 1: 冻结 2: 注销)
  ContractStatus: keyof typeof ContractStatus;
  // 合约状态
  ContractStatusText?: ValueOf<typeof ContractStatus>;
  // 合约类型
  ContractType: string;
  // 虚拟机类型
  RuntimeType: string;
  // 合约版本
  Version: string;
  // 创建交易
  TxId: string;
  // 创建用户ID
  CreateSender: string;
  // 创建用户地址
  CreatorAddr: string;
  /**
   * 创建者地址
   */
  CreatorAddrBns: string;
  // 发行量
  TotalSupply: string;
  // 持有人数
  HolderCount: number;
  // 创建时间
  CreateTimestamp: number;
  // 更新时间
  UpdateTimestamp: number;
  // 交易数
  TxNum: string;
}

export interface AccountDetail {
  // 账户地址
  Address: string;
  // DID
  DID: string;
  // 账户类型
  type: number;
  // BNS值
  BNS: string;
}

export interface GetNFTTransferListParam extends PagingWithChainId {
  // 合约地址
  ContractAddr: string;
  // 流转地址
  UserAddr?: string;
  /**
   * tokenlD
   */
  TokenId?: string;
}

export interface NFTTransferItem {
  // 交易ID
  TxId: string;
  // 合约名称
  ContractName: string;
  // 合约地址
  ContractAddr: string;
  // 合约方法
  ContractMethod: string;
  // from地址
  From: string;
  // to地址
  To: string;
  // tokenID
  TokenId: string;
  // 图片地址
  ImageUrl: string;
  // 上链时间
  Timestamp: number;
}

export interface GetNFTListParam extends PagingWithChainId {
  // tokenID
  TokenId?: string;
  // 合约地址/合约名称
  ContractKey?: string;
  // user地址集合，多个以逗号分割123,345
  OwnerAddrs?: string;
}

export interface NFTItem {
  // 持仓地址类型：0:用户地址，1:合约地址
  AddrType: number;
  // 合约地址
  ContractAddr: string;
  // 合约名称
  ContractName: string;
  // 持仓地址
  OwnerAddr: string;
  // tokenID
  TokenId: string;
  // 生成时间
  Timestamp: number;
  // 分组名称
  CategoryName: string;
  // 图片地址
  ImageUrl: string;
  // 地址类型 image/video
  UrlType: string;
}

export interface GetNFTDetailParam {
  // 链Id
  ChainId: string;
  // tokenID
  TokenId: string;
  // 合约地址
  ContractAddr: string;
}

export interface NFTDetail {
  // 合约名称
  ContractName: string;
  // 合约地址
  ContractAddr: string;
  // TokenId
  TokenId: string;
  // 持有地址
  OwnerAddr: string;
  // 持有地址类型
  AddrType: string;
  // 上链时间
  Timestamp: number;
  // 是否违规（true：违规）包括图片或文本违规
  IsViolation: boolean;
  Metadata: {
    // 作品名称
    name: string;
    // 作者
    author: string;
    // 图片地址
    imageUrl: string;
    // 地址类型 image/video
    urlType: string;
    // 作品描述
    description: string;
    // 作品Hash
    seriesHash: string;
  }
}
