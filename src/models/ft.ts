import { PagingWithChainId, ValueOf } from './common';
import { ContractStatus } from '@src/utils/enums';

export interface GetFTContractListParam extends PagingWithChainId {
  // 合约名称/合约地址
  ContractKey?: string;
}

export interface FTContractItem {
  // 合约名称
  ContractName: string;
  // 合约简称
  ContractSymbol: string;
  // 合约地址
  ContractAddr: string;
  // 合约类型
  ContractType: string;
  // 累计交易数
  TxNum: number;
  // 发行总量
  TotalSupply: string;
  // 持有人数
  HolderCount: number;
  // 创建时间
  Timestamp: number;

}

export interface GetFTPositionListParam extends PagingWithChainId {
  // 链ID
  ChainId: string;
  // 合约地址
  ContractAddr: string;
  // 持仓地址
  OwnerAddr?: string;
  // 页码
  Offset: number;
  // 每页条数
  Limit: number;
}

export interface FTPositionItem {
  // 持仓地址
  OwnerAddr: string;
  // 持有排名
  HoldRank: number;
  // 持有数量
  Amount: string;
  // 持有比例
  HoldRatio: string;
}

export interface GetFTContractDetailParam {
  // 链ID
  ChainId: string;
  // 合约地址
  ContractAddr: string;
}

export interface FTContractDetail {
  // 合约名称
  ContractName: string;
  // token简称
  ContractSymbol: string;
  // 合约地址
  ContractAddr: string;
  // 合约状态码
  ContractStatus: keyof typeof ContractStatus;
  // 合约状态
  ContractStatusText?: ValueOf<typeof ContractStatus>;
  // 合约类型
  ContractType: string;
  // 虚拟机类型
  RuntimeType: string;
  // 合约版本
  Version: string;
  // 创建交易
  TxId: string;
  // 创建用户ID
  CreateSender: string;
  // 创建用户地址
  CreatorAddr: string;
  /**
   * 创建者地址
   */
  CreatorAddrBns: string;
  // 发行量
  TotalSupply: string;
  // 持有人数
  HolderCount: number;
  // 创建时间
  CreateTimestamp: number;
  // 更新时间
  UpdateTimestamp: number;
  // 交易数
  TxNum: string;
}

export interface GetFTTransferListParam extends PagingWithChainId {
  // 合约地址
  ContractAddr: string;
  // 流转地址
  UserAddr?: string;
}

export interface FTTransferItem {
  // 交易ID
  TxId: string;
  // 合约名称
  ContractName: string;
  // 合约地址
  ContractAddr: string;
  // 合约方法
  ContractMethod: string;
  // 合约简称，token
  ContractSymbol: string;
  // from地址
  From: string;
  // to地址
  To: string;
  // 转账ETH
  Amount: string;
  // 上链时间
  Timestamp: number;
}
