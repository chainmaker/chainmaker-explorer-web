import { PagingWithChainId } from './common';

export interface GetEventListParam extends PagingWithChainId {
  // 合约地址
  ContractAddr?: string;
  /**
   * 合约名
   */
  ContractName?: string;
  // 交易ID
  TxId?: string;

  // 事件主题
  Topic?:string;
}

export interface EventItem {
  /**
   * 事件主题
   */
  Topic: string;
  /**
   * 事件信息
   */
  EventInfo: string;
  /**
   * 时间戳
   */
  Timestamp: number;
}
