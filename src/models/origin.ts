import { PagingWithChainId } from './common';

export interface GetOrgListParam extends PagingWithChainId {
  /**
   * 组织名
   */
  OrgId?: string;
}

export interface OriginItem {
  /**
   * 逻辑id
   */
  Id: number;
  /**
   * 组织id
   */
  OrgId: string;
  /**
   * 组织内的节点数
   */
  NodeCount: number;
  /**
   * 组织内的用户数
   */
  UserCount: number;
  /**
   * 时间戳
   */
  Timestamp: number;
}
