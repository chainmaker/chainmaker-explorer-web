import { ContractStatus } from '@src/utils/enums';
import { ParamWithChainId, ValueOf } from '.';
import { PagingWithChainId } from './common';

export type GetLatestContractListParam = ParamWithChainId;

/**
 * 合约类型
 */
export enum EnumContractType {
  CMDFA = 'CMDFA',
  CMNFA = 'CMNFA',
  CMID = 'CMID',
  CMEVI = 'CMEVI',
  Other = 'Other',
  IDA = 'IDA', // 数据要素
}

export interface GetLatestContractListItem {
  /**
   * 创建用户ID
   */
  Sender: string;
  /**
   * 创建用户地址
   */
  SenderAddr: string;
  /**
   * 合约地址
   */
  ContractAddr: string;
  /**
   * 合约名
   */
  ContractName?: string;
  /**
   * 当前版本号
   */
  Version: string;
  /**
   * 合约交易数量
   */
  TxNum: number;
  /**
   * 创建时间
   */
  Timestamp: number;
  /**
   * 合约类型
   */
  ContractType: EnumContractType;
}

export interface GetContractListParam extends PagingWithChainId {
  /**
   * 合约名称/合约地址
   */
  ContractKey?: string;
  /**
   * 发送用户名
   */
  Creators?: string;
  /**
   * 升序还是降序排列
   */
  // Order?: 'desc' | 'asc';
}

export interface VerifyContractSourceCodeParam {
  ChainId: string;
  ContractAddr: string;
  ContractVersion: string;
  CompilerPath: string;
  CompilerVersion: string;
  OpenLicenseType: string;
  ContractSourceFile: string;
  Optimization: boolean;
  Runs: number;
  EvmVersion: string;
}


export interface ContractItem {
  /**
   * 合约名
   */
  ContractName: string;
  /**
   * 当前版本号
   */
  Version: string;
  /**
   * 合约地址
   */
  Addr: string;
  /**
   * 合约交易数量
   */
  TxNum: number;
  /**
   * 创建者
   */
  Sender: string;
  /**
   * 创建者
   */
  Creator: string;
  /**
   * 创建者地址
   */
  SenderAddr: string;
  /**
   * 创建时间戳
   */
  CreateTimestamp: number;
  /**
   * 升级合约操作人
   */
  UpdateSender: string;
  /**
   * 升级时间戳
   */
  UpdateTimestamp: number;
  /**
   * 合约类型
   */
  ContractType: EnumContractType;

  /**
   * 合约验证状态
   */
  VerifyStatus: VerifyStatusEnum;
}

export interface GetContractDetailParam extends ParamWithChainId {
  /**
   * 合约名
   */
  ContractName: string;
}

export interface ContractInfo {
  /**
   * 合约名
   */
  ContractName: string;
  /**
   * 合约地址
   */
  ContractAddr: string;
  /**
   * 创建者地址
   */
  CreatorAddr: string;
  /**
   * 创建者地址
   */
  CreatorAddrBns: string;
  /**
   * 当前版本号
   */
  Version: string;
  /**
   * 交易发送者
   */
  CreateSender: string;
  /**
   * 创建交易id
   */
  TxId: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 升级合约操作人
   */
  UpgradeUser: string;
  /**
   * 升级时间戳
   */
  UpgradeTimestamp: number;
  /**
   * 合约状态码
   */
  ContractStatus: keyof typeof ContractStatus;
  /**
   * 合约状态
   */
  ContractStatusText?: ValueOf<typeof ContractStatus>;
  /**
   * 虚拟机类型
   */
  RuntimeType: string;
  /**
   * 合约类型
   */
  ContractType: EnumContractType;
  /**
   * 合约简称
   */
  ContractSymbol: string;
  /**
   * Token简称
   */
  TokenAbbreviation: string;
  /**
   * 总发行量
   */
  TotalSupply: string;

  /**
   * 资产登记总量
   */
  DataAssetNum: number;

  VerifyStatus: VerifyStatusEnum;

}

export interface GetContractCodeParam extends ParamWithChainId {
  /**
   * 合约名
   */
  ContractAddr?: string;

  /**
   * 合约版本
   */
  ContractVersion?: string;
}


export enum VerifyStatusEnum {
  /**
   * 未验证
   */
  Unverified = 0,
  /**
   * 通过验证
   */
  success = 1,
  /**
   * 未通过验证
   */
  fail = 2,
}

export const VerifyStatusTextMap:{
  [key in VerifyStatusEnum]: {
    text: string;
    theme: 'success' | 'danger' | 'text'
  }
} = {
  [VerifyStatusEnum.success]: {
    text: '验证成功',
    theme: 'success',
  },
  [VerifyStatusEnum.fail]: {
    text: '验证失败',
    theme: 'danger',
  },
  [VerifyStatusEnum.Unverified]: {
    text: '未验证',
    theme: 'text',
  },
};

export interface GetContractCodeRes {
  VerifyStatus: VerifyStatusEnum;
  MetaData: string;
  ContractName: string;
  ContractAddr: string;
  ContractVersion: string;
  CompilerVersion: string;
  EvmVersion: string;
  Optimization: boolean;
  Runs: number;
  OpenLicenseType: string;
  ContractAbi: string;
  SourceCodes: {
    SourcePath: string;
    SourceCode: string;
  }[];
}

export interface GetContractVersionListParam extends PagingWithChainId {
  /**
   * 合约名
   */
  ContractName?: string;
}

export interface GetContractVersionListItem {
  /**
   * 交易id
   */
  TxId: string;
  /**
   * 发起组织id
   */
  SenderOrgId: string;
  /**
   * 发起用户id
   */
  Sender: string;
  /**
   * 交易发送者地址
   */
  SenderAddr: string;
  /**
   * 交易发送者bns
   */
  SenderAddrBNS: string;
  /**
   * 升级版本号
   */
  Version: string;
  /**
   * 时间戳
   */
  Timestamp: number;
  /**
   * 0: 成功 1：失败
   */
  ContractResultCode: 0 | 1;
  TotalSupply: string;
  VerifyStatus: VerifyStatusEnum;
  ContractAddr: string;
  RuntimeType: string;
}
