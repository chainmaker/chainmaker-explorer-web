// import React, { useCallback, useEffect, useMemo, useState } from 'react';
// import liststyle from '../../list.module.scss';
// import { Table, TableColumn } from 'tea-component';
// import { EventItem } from '@src/models';
// import { ResponseList } from '@src/utils/apis';
// import { useParams } from 'react-router-dom';
// const { pageable } = Table.addons;

// export default function ListComponent({
//   columns,
//   getList,
//   recordKey,
// }: {
//   columns: TableColumn[];
//   getList: ({}) => ResponseList<any>;
//   recordKey: string;
// }) {
//   // const { chainId } = useParams();
//   // const [pageIndex, setPageIndex] = useState<number>(0);
//   const [recordCount, setRecordCount] = useState<number>(0);
//   const [list, setList] = useState<EventItem[]>([]);
//   const getListInfo = useCallback(
//     ({ pageIndex }) => {
//       // GetEventList({
//       //   ContractName: contractName,
//       // }).then((res) => {
//       //   if (res.GroupList?.length) {
//       //     setList(res.GroupList);
//       //     setRecordCount(res.TotalCount);
//       //   }
//       // });
//       getList(pageIndex).then((res) => {
//         setList(res.GroupList);
//         setRecordCount(res.TotalCount);
//       });
//     },
//     [chainId, pageIndex],
//   );
//   // const pageChange = useCallback(({ pageIndex }) => {
//   //   setPageIndex(pageIndex);
//   // }, []);
//   useEffect(() => {
//     getList();
//   }, [chainId]);
//   return (
//     <div className={liststyle.table}>
//       <Table
//         style={{
//           marginTop: 24,
//         }}
//         className={liststyle.table}
//         compact={false}
//         records={list}
//         recordKey={recordKey}
//          bordered={true}
//          disableTextOverflow={true}
//         columns={columns}
//         addons={[
//           pageable({
//             recordCount,
//             onPagingChange: (query) => getListInfo(query),
//           }),
//         ]}
//       />
//     </div>
//   );
// }
export {};
