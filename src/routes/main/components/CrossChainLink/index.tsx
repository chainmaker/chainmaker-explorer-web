import React from 'react';

export function CrossChainLink(props: { chainId: string | undefined; subChainId: string | undefined; subIsMainChain: boolean; name: string }) {
  const { chainId, subChainId, subIsMainChain, name } = props;
  if (subIsMainChain) {
    return <a href={`/${chainId}/home`}>{name}</a>;
  } else {
    return <a href={`/${chainId}/subchain/subChainDetail?subChainId=${subChainId}`}>{name}</a>;
  }
}