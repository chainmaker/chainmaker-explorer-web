import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import liststyle from '../list.module.scss';
import PageBox from '@src/components/ui/PageBox';
import { Button, Copy, Form, Justify, notification, Table, TableColumn, Text } from 'tea-component';
import SearchInput from '@components/ui/SearchInput';
import SubscribeFormModel from './components/SubscribeFormModel';
import ConfirmModal from '@components/ui/ConfirmModal';
import { DeleteSubscribe, GetChainList, ModifySubscribe, SubscribeChain } from '@src/utils/apis';
import { ChainItem, Subscribe } from '@src/models';
import { formatUnixTime } from '@src/utils/tools';
import { useDispatch, useSelector } from 'react-redux';
import { RootReducer } from '@src/store';
import { chainStatusMap } from '@src/utils/enums';

const { pageable, autotip } = Table.addons;

function ChainConfigForm({ NodeAddrList, ChainConfig }: Pick<ChainItem, 'NodeAddrList' | 'ChainConfig'>) {
  const editorElRef = useRef<any>(null);
  const jsonFormatted = useMemo(() => {
    return JSON.stringify(JSON.parse(ChainConfig), null, 2);
  }, [ChainConfig]);
  useEffect(() => {
    const editor = window.monaco.editor.create(editorElRef.current, {
      autoIndent: 'full',
      automaticLayout: true,
      contextmenu: false,
      language: 'json',
      minimap: { enabled: false },
      readOnly: true,
      scrollbar: {
        verticalScrollbarSize: 10,
        horizontalScrollbarSize: 10,
      },
      scrollBeyondLastLine: false,
      theme: 'vs-dark',
    });
    editor.setValue(jsonFormatted);
  }, [jsonFormatted]);

  return <Form layout={'vertical'}>
    <Form.Item label={'节点IP'}>
      <Form.Text>
        {NodeAddrList.join(' ，')}
      </Form.Text>
    </Form.Item>
    <Form.Item label={<>
      链配置JSON
      <Copy text={ChainConfig}/>
    </>}>
      <Form.Text>
        <div className={'chain-config-json-formatter'} ref={editorElRef}/>
      </Form.Text>
    </Form.Item>
  </Form>;
}

export default function Chain() {
  const dispatch = useDispatch();
  const { listParams } = useSelector((state: RootReducer) => state.chainReducer);
  const [subTitle, setSubTitle] = useState('');
  const [visible, setVisible] = useState(false);
  const [initialData, setInitiaData] = useState<any>('');

  const [data, setData] = useState<{
    total: number;
    list: ChainItem[];
  }>({
    total: 0,
    list: [],
  });

  const [confirmVisible, setConfirmVisible] = useState<boolean>(false);
  const [editChainId, setEditChainId] = useState<string>('');
  const [isLoading, setIsLoading] = useState(false);
  const updateChainSelect = useCallback(() => {
    dispatch({
      type: 'GET_CHAINS',
      payload: {
        Limit: 99999,
        Offset: 0,
      },
    });
  }, [dispatch]);

  const updateparam = useCallback((payload) => {
    dispatch({
      type: 'UPDATE_CHAINS_LIST_PARAM',
      payload,
    });
  }, [dispatch]);

  const onSearch = useCallback((newParams = {}) => {
    setIsLoading(true);
    const updateParams = {
      ...listParams,
      ...newParams,
    };
    GetChainList({
      ChainId: updateParams.ChainId,
      Offset: updateParams.Offset,
      Limit: updateParams.Limit,
    })
      .then((res) => {
        setIsLoading(false);
        if (res.GroupList) {
          setData({
            list: res.GroupList,
            total: res.TotalCount,
          });
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [listParams]);

  const onSearchRef = useRef(onSearch);
  onSearchRef.current = onSearch;

  const openAdd = useCallback(() => {
    setSubTitle('新增订阅链');
    setInitiaData('');
    setVisible(true);
  }, []);
  const openEdit = useCallback((data, isReEdit?: boolean) => {
    setSubTitle(isReEdit ? '重新订阅链信息' : '修改订阅链信息');
    setInitiaData(data);
    setVisible(true);
  }, []);
  const onSubmit = useCallback(
    (subscribe: Subscribe) => {
      const subapi = initialData ? ModifySubscribe : SubscribeChain;
      return subapi(subscribe).then((res: any) => {
        if (res.Error) {
          onSearch();
          setVisible(false);
          return;
        }
        notification.success({
          title: '订阅成功',
        });
        onSearch();
        updateChainSelect();
        setVisible(false);
      });
    },
    [initialData, onSearch, updateChainSelect],
  );
  const openCancelConfirm = useCallback((Id) => {
    setConfirmVisible(true);
    setEditChainId(Id);
  }, []);
  const subCancelSubscribe = useCallback(() => {
    return DeleteSubscribe({
      ChainId: editChainId,
    }).then((res: any) => {
      setConfirmVisible(false);
      if (!res.Error) {
        onSearch();
        // updateChainSelect();
      }
    });
  }, [editChainId, onSearch]);

  const columns = useMemo<TableColumn<ChainItem>[]>(
    () => [
      {
        key: 'ChainId',
        header: '区块链id',
        align: 'left',
      },
      {
        key: 'ChainVersion',
        header: '链版本',
        align: 'left',
      },
      {
        key: 'AuthType',
        header: '链账户模式',
        align: 'left',
      },
      {
        key: 'Consensus',
        header: '共识算法',
        align: 'left',
      },
      {
        key: 'Timestamp',
        header: '创建时间',
        align: 'left',
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
      },
      {
        key: 'Status',
        header: '状态',
        align: 'left',
        render: ({ Status }) => <Text theme={chainStatusMap[Status].theme}>{chainStatusMap[Status].text}</Text>,
      },
      {
        key: 'Id',
        header: '操作',
        align: 'center',
        width: 200,
        render: ({ ChainId, AuthType, Status, NodeAddrList, ChainConfig }) => (
          <div className={liststyle.setting_c}>
            <Button type={'link'} disabled={Status !== 0} onClick={() => {
              dispatch({
                type: 'show_confirm_modal',
                payload: {
                  caption: '链配置',
                  body: <ChainConfigForm NodeAddrList={NodeAddrList} ChainConfig={ChainConfig}/>,
                  footerButtons: ({ close }: { close: any }) => {
                    return <Button type={'primary'} onClick={() => {
                      close();
                    }}>
                        确认
                    </Button>;
                  },
                },
              });
            }}>
                查看
            </Button>
            {Status !== 2 && (
              <Text theme="primary" onClick={() => openEdit({ ChainId, AuthType })}>
                  重新订阅
              </Text>
            )}
            {/* {Status === 2 ? (
              <Text theme="primary" onClick={() => openEdit(ChainId, true)}>
                重新订阅
              </Text>
            ) : (
              <Text theme="primary" onClick={() => openEdit(ChainId)}>
                修改
              </Text>
            )} */}
            {Status !== 2 && (
              <>
                <Text theme="warning" onClick={() => openCancelConfirm(ChainId)}>
                    删除
                </Text>
              </>
            )}
          </div>
        ),
      },
    ],
    [],
  )
  ;

  useEffect(() => {
    onSearchRef.current();
  }, []);

  return (
    <PageBox title="区块链列表">
      <Justify
        left={
          <Button style={{ height: 40, width: 115, fontSize: 14 }} onClick={() => openAdd()} type="primary">
            新增订阅
          </Button>
        }
        right={
          <SearchInput
            placeholder="请输入链Id搜索"
            onChange={(value) => updateparam({
              ChainId: value,
            })}
            onSubmit={onSearch}
            defaultValue={listParams.ChainId}
          />
        }
      />
      <div className={liststyle.table}>
        <Table
          style={{
            marginTop: 24,
          }}
          className={liststyle.table}
          compact={false}
          records={data.list}
          recordKey="Id"
          bordered={true}
          disableTextOverflow={true}
          columns={columns}
          addons={[
            autotip({
              isLoading,
            }),
            pageable({
              recordCount: data.total,
              pageIndex: listParams.Offset! + 1,
              onPagingChange: (query) => {
                onSearchRef.current({
                  Limit: query.pageSize,
                  Offset: query.pageIndex! - 1,
                });
              },
            }),
          ]}
        />
      </div>
      <SubscribeFormModel
        visible={visible}
        onSubmit={onSubmit}
        onClose={() => setVisible(false)}
        initialData={initialData}
        title={subTitle}
      />
      <ConfirmModal
        visible={confirmVisible}
        onSubmit={subCancelSubscribe}
        onClose={() => setConfirmVisible(false)}
        title="删除订阅确认"
        message={`确定要删除订阅${editChainId}吗？删除订阅后，浏览器将无法再获取到该链的信息，且将清空原有数据，如后续还想获得信息，需要重新订阅，请确定是否删除订阅。`}
        submitText="确定删除"
        cancelText="我在想想"
      />
    </PageBox>
  );
};
