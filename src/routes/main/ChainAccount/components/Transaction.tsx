import React, { useCallback, useEffect, useMemo, useState } from 'react';
import liststyle from '../../list.module.scss';
import { Bubble, Table, TableColumn, Text } from 'tea-component';
import { Tx } from '@src/models';
import { GetUserTxList } from '@src/utils/apis';
import { Link } from 'react-router-dom';
import { formatUnixTime } from '@src/utils/tools';
import { RootReducer } from '@src/store';
import { useSelector } from 'react-redux';
import { TxStatusMap } from '@src/constant';
import { autotip } from 'tea-component/es/table/addons';

const { pageable } = Table.addons;

export default function Transaction({ name }: { name?: string }) {
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const authType = useMemo(() => currentChain?.AuthType, [currentChain]);
  const [pageIndex, setPageIndex] = useState<number>(1);
  const [pageSize, setPageSize] = useState<number>(10);
  const [recordCount, setRecordCount] = useState<number>(0);
  const [list, setList] = useState<Tx[]>([]);
  const getList = useCallback(() => {
    if (!name) {
      return;
    }
    GetUserTxList({
      Offset: pageIndex - 1,
      ChainId: chainId,
      UserAddrs: name,
      Limit: pageSize,
    }).then((res) => {
      if (res.GroupList) {
        setList(res.GroupList);
        setRecordCount(res.TotalCount);
      }
    });
  }, [chainId, name, pageIndex, pageSize]);
  useEffect(() => {
    getList();
  }, [name, chainId, pageIndex, pageSize]);
  const columns = useMemo<TableColumn[]>(() => {
    const list: TableColumn<Tx>[] = [
      {
        key: 'BlockHeight',
        header: '区块高度',
        align: 'left',
        render: ({ BlockHeight, BlockHash }) => <Link to={`/${chainId}/block/${BlockHash}`}>{BlockHeight}</Link>,
      },
      {
        key: 'TxId',
        header: '交易Id',
        align: 'left',
        width: 320,
        render: ({ TxId }) => <Link to={`/${chainId}/transaction/${TxId}`}>{TxId}</Link>,
      },
      {
        key: 'Sender',
        header: '发起用户',
        align: 'left',
        render:
          authType === 'permissionedwithcert'
            ? ({ Sender, UserAddr }) => <Bubble content={UserAddr}>{Sender}</Bubble>
            : ({ UserAddr }) => UserAddr || '--',
      },
      {
        key: 'ContractMethod',
        header: '调用方法',
        align: 'left',
      },
      {
        key: 'TxStatusCode',
        header: '交易状态',
        align: 'left',
        width: 100,
        render: ({ TxStatus }) => (
          <Text theme={TxStatusMap[TxStatus].theme}>{TxStatusMap[TxStatus].text}</Text>
        ),
      },
      {
        key: 'Timestamp',
        header: '上链时间',
        align: 'left',
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
        width: 180,
      },
    ];
    if (authType === 'permissionedwithcert') {
      list.splice(2, 0, {
        key: 'SenderOrgId',
        header: '发起组织',
        align: 'left',
      });
    }
    return list;
  }, []);
  return (
    <Table
      className={liststyle.table}
      compact={false}
      records={list}
      recordKey="TxId"
      bordered={true}
      disableTextOverflow={true}
      columns={columns}
      addons={[
        autotip({
        }),
        pageable({
          recordCount,
          onPagingChange: (query) => {
            if (query?.pageIndex) {
              setPageIndex(query.pageIndex);
            }
            if (query?.pageSize) {
              setPageSize(query.pageSize);
            }
          },
        }),
      ]}
    />
  );
}
