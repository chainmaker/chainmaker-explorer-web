import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import liststyle from '../list.module.scss';
import PageBox from '@src/components/ui/PageBox';
import { Text, Justify, Table, TableColumn, Bubble, Form, Input, Button } from 'tea-component';
import { BlockItem } from '@src/models';
import { GetBlockList } from '@src/utils/apis';
import { Link } from 'react-router-dom';
import { formatUnixTime } from '@src/utils/tools';
import { useDispatch, useSelector } from 'react-redux';
import { RootReducer } from '@src/store';
import { useQuery } from '@utils/utils';

const { pageable, autotip } = Table.addons;

export default function Block() {
  const urlSearchParams = useQuery();
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const dispatch = useDispatch();
  const blockListParamState = useSelector((state: RootReducer) => state.blockListParamReducer);
  const updateparam = useCallback((payload) => {
    dispatch({
      type: 'UPDATE_BLOCKLIST_PARAM',
      payload,
    });
  }, [dispatch]);
  const [recordCount, setRecordCount] = useState<number>(0);
  const [list, setList] = useState<BlockItem[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const getList = useCallback((params = {}) => {
    updateparam(params);
    const mergeParams = {
      ...blockListParamState,
      ...params,
    };
    setIsLoading(true);
    GetBlockList({
      BlockKey: mergeParams.value?.trim(),
      ChainId: chainId,
      Limit: mergeParams.pageSize,
      Offset: mergeParams.pageIndex - 1,
      NodeId: mergeParams.nodeId?.trim(),
    })
      .then((res) => {
        setIsLoading(false);
        if (res.GroupList) {
          setList(res.GroupList);
          setRecordCount(res.TotalCount);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [updateparam, blockListParamState, chainId]);

  const searchRef = useRef(getList);
  searchRef.current = getList;

  const onSearch = useCallback(() => {
    searchRef.current({ pageIndex: 1 });
  }, []);

  useEffect(() => {
    searchRef.current({
      nodeId: urlSearchParams.get('nodeId'),
    });
  }, []);

  const columns = useMemo<TableColumn[]>(
    () => [
      {
        key: 'BlockHeight',
        header: '区块高度',
        align: 'left',
        width: 100,
        render: ({ BlockHeight, BlockHash }) => <Link to={`/${chainId}/block/${BlockHash}`}>{BlockHeight}</Link>,
      },
      {
        key: 'BlockHash',
        header: '区块Hash',
        align: 'left',
        render: ({ BlockHash }) => <Link to={`/${chainId}/block/${BlockHash}`}>{BlockHash}</Link>,
      },
      {
        key: 'TxNum',
        header: '交易数',
        align: 'left',
        width: 100,
      },
      {
        key: 'ProposalNodeId',
        header: '出块节点',
        align: 'left',
        render: ({ ProposalNodeId, ProposalNodeAddr }) => ProposalNodeId && <Bubble content={ProposalNodeAddr}>
          <Link to={`/${chainId}/node`} onClick={() => {
            dispatch({
              type: 'UPDATE_NODELIST_PARAM',
              payload: {
                nodeId: ProposalNodeId,
              },
            });
          }}>
            {ProposalNodeId}
          </Link>
        </Bubble>,
      },
      {
        key: 'Timestamp',
        header: '区块生成时间',
        align: 'left',
        width: 200,
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
      },
      {
        key: 'Id',
        header: '操作',
        align: 'center',
        width: 160,
        render: ({ BlockHash }) => (
          <div className={liststyle.setting_c}>
            <Link to={`/${chainId}/block/${BlockHash}`}>
              <Text theme="primary">查看</Text>
            </Link>
          </div>
        ),
      },
    ],
    [],
  );
  return (
    <PageBox title="区块列表">
      <Justify
        right={
          <Form className={liststyle.searchform} hideLabel={false} layout="inline">
            <Form.Item label="出块节点">
              <Input
                onChange={(value) => updateparam({ nodeId: value })}
                value={blockListParamState.nodeId}
                placeholder="请输入节点id"
              />
            </Form.Item>
            <Form.Item label="区块哈希">
              <Input
                onChange={(value) => updateparam({ value })}
                value={blockListParamState.value}
                placeholder="请输入区块哈希/区块号搜索"
              />
            </Form.Item>
            <Form.Item label="">
              <Button type="primary" onClick={onSearch}>
                搜索
              </Button>
            </Form.Item>
          </Form>
        }
      />
      <Table
        className={liststyle.table}
        compact={false}
        records={list}
        recordKey="BlockHash"
        bordered={true}
        disableTextOverflow={true}
        columns={columns}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            recordCount,
            pageIndex: blockListParamState.pageIndex,
            pageSize: blockListParamState.pageSize,
            onPagingChange: (query) => {
              searchRef.current(query);
            },
          }),
        ]}
      />
    </PageBox>
  );
}
