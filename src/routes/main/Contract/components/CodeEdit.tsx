import React, { useEffect } from 'react';
import style from '../Detail/index.module.scss';
import { useParams } from 'react-router-dom';
import { ContractInfo, FTContractDetail, NFTContractDetail, VerifyStatusEnum } from '@src/models';
import { Button } from 'tea-component';
import { useGetContractCode, useNavigateChainChildPath } from '@utils/hooks';
import { ContractVerifyResultForm } from '@src/routes/main/Contract/verify/ContractVerifyResultPage';
import { qsUtils } from '@utils/utils';

/**
 * 合约代码
 */
export default function CodeEdit({ contract }: {
  contract: ContractInfo | FTContractDetail | NFTContractDetail | null;
}) {
  const { chainId } = useParams();
  const navigate = useNavigateChainChildPath();
  const { fetch, code } = useGetContractCode();
  useEffect(() => {
    if (!contract || !chainId
      || contract?.RuntimeType !== 'EVM'
      || ((contract as ContractInfo)?.VerifyStatus !== VerifyStatusEnum.success)) {
      return;
    }
    fetch({
      chainId,
      contractAddr: contract.ContractAddr,
      contractVersion: contract.Version,
    });
  }, [chainId, fetch, contract]);

  if (contract?.RuntimeType !== 'EVM') {
    return <>
      <div className="empty_list"></div>
      <div className={style.empty_word}>暂未提供合约源码</div>
    </>;
  }

  return (
    <>
      {
        (code?.VerifyStatus !== VerifyStatusEnum.success) ?
          <>
            <div className="empty_list"></div>
            <div className={style.empty_word}>
              <div>暂未提供合约源码</div>
              <Button type={'primary'} className={'tea-mt-2n'} onClick={() => {
                navigate(`/contract/verify?${qsUtils.stringify({
                  contractVersion: contract?.Version,
                  contractAddr: contract?.ContractAddr,
                })}`);
              }}>合约验证</Button>
            </div>
          </>
          : <ContractVerifyResultForm code={code}/>
      }
    </>
  );
}
