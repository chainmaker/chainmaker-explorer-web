import React, { useCallback, useContext, useEffect, useImperativeHandle, useMemo, useRef, useState } from 'react';
import liststyle from '../../list.module.scss';
import { Button, Form, Input, Justify, Select, Table, TableColumn, Text } from 'tea-component';
import { Tx } from '@src/models';
import { GetContractTxList, GetQueryTxList } from '@src/utils/apis';
import { Link } from 'react-router-dom';
import { formatUnixTime } from '@src/utils/tools';
import { RootReducer } from '@src/store';
import { useSelector } from 'react-redux';
import { TxStatusMap, TxStatusOptions } from '@src/constant';
import { UserAddress } from '@src/components/ui/AttrShow/UserAddress';
import { autotip } from 'tea-component/es/table/addons';
import { AppContext } from '@components/context';
import { RangePicker } from 'tea-component/es/datepicker/RangePicker';
import { Controller, useForm } from 'react-hook-form';
import { ResponseList } from '@utils/apis/type';
import GlossaryGuide from '@components/glossary-guide';
// import SearchInput from '@components/ui/SearchInput';
const { pageable } = Table.addons;
const ADVANCED_SEARCH_BOOL_OPTIONS = [
  { text: 'And', value: 'and' },
  { text: 'Or', value: 'or' },
];
const SEARCH_OPTIONS = [
  {
    value: 'UserAddrs',
    text: '账户地址',
  },
  {
    value: 'ContractMethod',
    text: '调用方法',
  },
];

const AdvancedSearchContainer = ({
  children,
}: {
  children: React.ReactNode;
}, ref: React.Ref<any>) => {
  const {
    getValues,
    control,
    setValue,
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      ContractAddr: '',
      ContractMethod: '',
      TxId: '',
      TxStatus: '-1',
      UserAddr: '',
      Operator: 'and',
      StartTime: null,
      EndTime: null,
    },
  });

  useImperativeHandle(ref, () => ({
    getValues,
  }), [getValues]);

  return <>
    <div
      className={'tea-mt-5n advanced-search-form'}>
      <Form layout={'inline'}>
        <Controller control={control} render={
          ({ field }) => {
            return <Select options={ADVANCED_SEARCH_BOOL_OPTIONS} {...field}/>;
          }
        } name={'Operator'}/>
        <Controller control={control} render={({ field }) =>
          <Form.Item label={'账户地址'}><Input placeholder={'请输入账户地址'} {...field}/></Form.Item>}
        name={'UserAddr'}/>
        <Controller control={control} render={({ field }) =>
          <Form.Item label={'调用方法'}><Input placeholder={'请输入调用方法'} {...field}/></Form.Item>}
        name={'ContractMethod'}/>
        <Controller control={control} render={({ field }) =>
          <Form.Item label={'内容关键字（交易Id）'}> <Input placeholder={'请输入内容关键字'} {...field}/>
          </Form.Item>} name={'TxId'}/>
        <Controller control={control} render={({ field }) => <Form.Item label="交易状态"><Select
          options={TxStatusOptions}
          defaultValue={'-1'}
          clearable
          matchButtonWidth
          appearance="button"
          size="m" {...field}
        /> </Form.Item>} name={'TxStatus'}/>
      </Form>
      <Form layout={'inline'} className={'tea-mt-2n'}>
        <Controller control={control} render={({}) => <Form.Item
          label={<GlossaryGuide title={'上链时间'} popover={'以 And 条件处理上链时间范围'}/>}>
          <RangePicker clearable showTime onChange={value => {
            // @ts-ignore
            setValue('StartTime', value[0]?.unix());
            // @ts-ignore
            setValue('EndTime', value[1]?.unix());
          }}/></Form.Item>} name={'StartTime'}/>
        {children}
      </Form>
    </div>
  </>;
};
const AdvancedSearch = React.forwardRef(AdvancedSearchContainer);

export default function Transaction({ name }: { name?: string }) {
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const authType = useMemo(() => currentChain?.AuthType, [currentChain]);
  const [recordCount, setRecordCount] = useState<number>(0);
  const [list, setList] = useState<Tx[]>([]);
  const [searchType, setSearchType] = useState<string>('UserAddrs');
  const [searchValue, setSearchValue] = useState<string>('');
  const [isLoading, setIsLoading] = useState(false);
  const {
    appConfig: {
      APP_IS_SHOW_TRANSACTION_ADVANCED_SEARCH,
    },
  } = useContext(AppContext);
  const advancedSearchRef = useRef<{ getValues: () => any }>(null);
  const [queryParams, setQueryParams] = useState({
    pageIndex: 1,
    pageSize: 10,
  });
  const getList = useCallback((page = {}) => {
    const mergedQuery = { ...queryParams, ...page };
    setQueryParams(mergedQuery);
    if (!name) {
      return;
    }
    setIsLoading(true);
    let promise: Promise<ResponseList<Tx>>;
    if (advancedSearchRef.current) {
      let params = advancedSearchRef.current.getValues();
      params = {
        ...params,
        ContractName: name,
        ChainId: chainId,
        Offset: mergedQuery.pageIndex - 1,
        Limit: mergedQuery.pageSize,
        StartTime: params.StartTime,
        EndTime: params.EndTime,
      };
      console.log(params, 'params');
      promise = GetQueryTxList(params);
    } else {
      const params: {
        Offset: number;
        ChainId: string | undefined;
        ContractName: string;
        Limit: number;
        UserAddrs?: string;
        ContractMethod?: string;
      } = {
        Offset: mergedQuery.pageIndex - 1,
        ChainId: chainId,
        ContractName: name,
        Limit: mergedQuery.pageSize,
      };

      if (searchType === 'UserAddrs') {
        params.UserAddrs = searchValue;
      }
      if (searchType === 'ContractMethod') {
        params.ContractMethod = searchValue;
      }
      promise = GetContractTxList(params);
    }
    promise.then((res) => {
      if (res.GroupList) {
        setList(res.GroupList);
        setRecordCount(res.TotalCount);
      }
    }).finally(() => {
      setIsLoading(false);
    });
  }, [chainId, name, searchType, searchValue, queryParams]);

  const getListRef = useRef(getList);

  getListRef.current = getList;

  useEffect(() => {
    getListRef.current();
  }, [name]);

  const columns = useMemo<TableColumn[]>(() => {
    const list: TableColumn<Tx>[] = [
      {
        key: 'Timestamp',
        header: '上链时间',
        align: 'left',
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
        width: 180,
      },
      {
        key: 'TxId',
        header: '交易Id',
        align: 'left',
        width: 220,
        render: ({ TxId }) => <Link to={`/${chainId}/transaction/${TxId}`}>{TxId}</Link>,
      },
      {
        key: 'Sender',
        header: '发起用户',
        align: 'left',
        render: ({ UserAddrBns, Sender, UserAddr }) => <UserAddress UserAddrBns={UserAddrBns} Sender={Sender}
          UserAddr={UserAddr}/>,
      },
      {
        key: 'ContractMethod',
        header: '调用方法',
        align: 'left',
      },
      {
        key: 'TxStatus',
        header: '交易状态',
        align: 'left',
        width: 100,
        render: ({ TxStatus }) => (
          <Text theme={TxStatusMap[TxStatus].theme}>{TxStatusMap[TxStatus].text}</Text>
        ),
      },
      {
        key: 'Id',
        header: '操作',
        align: 'center',
        width: 100,
        render: ({ TxId }) => <Link to={`/${chainId}/transaction/${TxId}`}>查看</Link>,
      },
    ];
    if (authType === 'permissionedwithcert') {
      list.splice(2, 0, {
        key: 'SenderOrgId',
        header: '发起组织',
        align: 'left',
      });
    }
    return list;
  }, [authType, chainId]);
  return (
    <>
      {
        APP_IS_SHOW_TRANSACTION_ADVANCED_SEARCH ?
          <AdvancedSearch ref={advancedSearchRef}>
            <Button className="tea_cover_button_primary" onClick={() => {
              getListRef.current({
                pageIndex: 1,
              });
            }}>搜索</Button>
          </AdvancedSearch>
          : <Justify
            className={'tea-mt-5n'} right={<>
              <Select
                className="tea_cover_select"
                appearance="button"
                matchButtonWidth
                options={SEARCH_OPTIONS}
                value={searchType}
                onChange={value => setSearchType(value)}
              />
              <Input className="tea_cover_input" placeholder="输入账户地址/调用方法搜索" value={searchValue}
                onChange={value => setSearchValue(value)}/>
              <Button className="tea_cover_button_primary" onClick={() => {
                getListRef.current({
                  pageIndex: 1,
                });
              }}>搜索</Button>
            </>
            }/>
      }
      <Table
        className={liststyle.table}
        compact={false}
        records={list}
        recordKey="TxId"
        bordered={true}
        disableTextOverflow={true}
        columns={columns}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            pageSize: queryParams.pageSize,
            pageIndex: queryParams.pageIndex,
            recordCount,
            onPagingChange: (query) => {
              getListRef.current(query);
            },
          }),
        ]}
      />
    </>
  );
}
