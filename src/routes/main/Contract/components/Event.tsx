import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import liststyle from '../../list.module.scss';
import { Button, Input, Justify, Table, TableColumn } from 'tea-component';
import { EventItem } from '@src/models';
import { GetEventList } from '@src/utils/apis';
import { useParams } from 'react-router-dom';
import { formatUnixTime } from '@src/utils/tools';

const { pageable, autotip } = Table.addons;

export default function Event({ name }: { name?: string }) {
  const { chainId } = useParams();

  const [recordCount, setRecordCount] = useState<number>(0);
  const [isLoading, setIsLoading] = useState(false);
  const [list, setList] = useState<EventItem[]>([]);
  const eventInputRef = useRef<HTMLInputElement>(null);

  const [queryParams, setQueryParams] = useState({
    pageIndex: 1,
    pageSize: 10,
    topic: '',
  });

  const getList = useCallback(() => {
    if (!name) {
      return;
    }
    setIsLoading(true);
    GetEventList({
      Offset: queryParams.pageIndex - 1,
      Limit: queryParams.pageSize,
      ChainId: chainId,
      ContractName: name,
      Topic: queryParams.topic,
    })
      .then((res) => {
        setIsLoading(false);
        if (res.GroupList) {
          setList(res.GroupList);
          setRecordCount(res.TotalCount);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [chainId, name, queryParams]);
  useEffect(() => {
    getList();
  }, [getList]);
  const columns = useMemo<TableColumn[]>(
    () => [
      {
        key: 'Timestamp',
        header: '事件时间',
        width: 190,
        align: 'left',
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
      },
      {
        key: 'Topic',
        header: '事件主题',
        width: 210,
        align: 'left',
      },
      {
        key: 'EventInfo',
        header: '事件内容',
        align: 'left',
        render: ({ EventInfo }) => <div className={'contract-event__table-content-column'}>{EventInfo}</div>,
      },
    ],
    [],
  );
  return (
    <>
      <Justify
        className={'tea-mt-5n'}
        right={
          <>
            <Input placeholder="请输入事件主题" ref={eventInputRef} onPressEnter={(value)=>{
              setQueryParams(state => ({
                ...state,
                pageIndex: 1,
                topic: value.trim(),
              }));
            }}/>
            <Button type={'primary'} onClick={() => {
              setQueryParams(state => ({
                ...state,
                pageIndex: 1,
                topic: eventInputRef.current ? eventInputRef.current.value.trim() : '',
              }));
            }}>搜索</Button>
          </>
        }/>
      <Table
        className={liststyle.table}
        compact={false}
        records={list}
        bordered={true}
        disableTextOverflow={true}
        columns={columns}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            recordCount,
            pageIndex:queryParams.pageIndex,
            pageSize: queryParams.pageSize,
            onPagingChange: (query) => {
              setQueryParams(state => ({
                ...state,
                ...query,
              }));
            },
          }),
        ]}
      />
    </>
  );
}
