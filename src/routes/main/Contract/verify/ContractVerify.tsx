import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PageBox from '@components/ui/PageBox';
import { Button, Form, Icon, Input, message, Select, Text } from 'tea-component';
import { Controller, useForm, useWatch } from 'react-hook-form';
import { qsUtils, useQuery } from '@utils/utils';
import { useNavigate } from 'react-router-dom';
import {
  GetCompilerVersions,
  GetContractVersions,
  GetEvmVersions,
  GetOpenLicenseTypes,
  VerifyContractSourceCode,
} from '@utils/apis';
import { useCurrentChainId } from '@utils/hooks';
import { SelectOption } from 'tea-component/lib/select/SelectOption';
import { VerifyStatusEnum } from '@src/models';
import FileUpload from '@components/form/FileUpload';
import { useDispatch } from 'react-redux';
import JSZip from 'jszip';
import { monaco_options } from '@src/constant';
import GlossaryGuide from '@components/glossary-guide';

const optimization_options = [
  {
    text: 'true',
    value: 'true',
  },
  {
    text: 'false',
    value: 'false',
  },
];

const ContractVerifyError = ({ res }: {
  res: {
    VerifyStatus: VerifyStatusEnum;
    Message: string;
    MetaData?: string;
  }
}) => {
  const contractABIElRef = React.useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (res.MetaData && contractABIElRef.current) {
      const editor = window.monaco.editor.create(contractABIElRef.current, {
        ...monaco_options,
        language: 'json',
        wordWrap: 'on',
        readOnly: true,
      });
      editor.setValue(res.MetaData);
    }
  }, [res.MetaData]);

  return <Form layout={'vertical'}>
    <Form.Item label={'错误原因'}>
      <Form.Text>{res.Message}</Form.Text>
    </Form.Item>
    {
      res.MetaData &&
      <Form.Item label={'MetaData'}>
        <Form.Text>
          <div ref={contractABIElRef} className={'monaco-editor-metadata'}>
          </div>
        </Form.Text>
      </Form.Item>}
  </Form>;
};

const CompilerPathTip = <>
  程序执行入口路径，例如路径为 <Text theme={'primary'}>contract/MainContract.sol:MainContract</Text>
  。如果是 zip 文件上传，则文件夹路径需包含 zip 解压后的文件夹路径；如果是多 sol 文件上传，则不需要。
</>;

/**
 * 合约验证
 */
export function ContractVerify() {
  const queryParams = useQuery();
  const chainId = useCurrentChainId();
  const [loading, setLoading] = useState(false);
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const defaultValues = useMemo(() => ({
    ContractAddr: queryParams.get('contractAddr') || '',
    ContractVersion: queryParams.get('contractVersion') || '',
    CompilerPath: '',
    CompilerVersion: '',
    OpenLicenseType: 'No License (None)',
    ContractSourceFile: '',
    Optimization: 'true',
    Runs: '200',
    EvmVersion: '',
  }), [queryParams]);
  const {
    control,
    reset,
    setValue,
    getValues,
    formState: {
      isValid,
    },
  } = useForm({
    mode: 'onChange',
    defaultValues: defaultValues,
  });

  const contractAddrWatch = useWatch({
    name: 'ContractAddr',
    control,
  });

  const handleReset = useCallback(() => {
    reset(defaultValues);
  }, [defaultValues, reset]);

  const handleSubmit = useCallback(async () => {
    setLoading(true);
    const values = getValues();
    if (Array.isArray(values.ContractSourceFile)) {
      if (values.ContractSourceFile[0].type === 'application/zip') {
        values.ContractSourceFile = values.ContractSourceFile[0];
      } else {
        const zip = new JSZip();
        values.ContractSourceFile.forEach((file) => {
          zip.file(file.name, file);
        });
        const blob = await zip.generateAsync({ type: 'blob' });
        // @ts-ignore
        values.ContractSourceFile = new File([blob], 'contract.zip');
      }
    }
    VerifyContractSourceCode({
      ChainId: chainId,
      ...values,
      Runs: (+values.Runs) ?? 200,
      Optimization: values.Optimization === 'true',
    }).then((res) => {
      if (res.Data.VerifyStatus === VerifyStatusEnum.success) {
        message.success({
          content: '验证成功',
          duration: 1500,
          onClose: () => {
            navigate(`./result?${qsUtils.stringify({
              contractVersion: values?.ContractVersion,
              contractAddr: values?.ContractAddr,
            })}`);
          },
        });
      } else {
        dispatch({
          type: 'show_confirm_modal',
          payload: {
            caption: <>
              <Icon type={'error'} className={'tea-mr-1n'}/>验证失败</>,
            body: <ContractVerifyError res={res.Data}/>,
            footerButtons: ({ close }: { close: any }) => {
              return <Button type={'primary'} onClick={() => {
                close();
              }}>
                确认
              </Button>;
            },
          },
        });
      }
    }).finally(() => {
      setLoading(false);
    });
  }, [chainId, getValues, navigate]);
  const [contractVersions, setContractVersions] = useState<SelectOption[]>([]);
  const [compilerVersions, setCompilerVersions] = useState<SelectOption[]>([]);
  const [licenseOptions, setLicenseOptions] = useState<SelectOption[]>([]);
  const [evmVersions, setEvmVersions] = useState<SelectOption[]>([]);

  useEffect(() => {
    const add = contractAddrWatch.trim();
    if (!add) {
      setContractVersions([]);
      setValue('ContractVersion', '');
      return;
    }
    GetContractVersions({
      ContractAddr: add,
      ChainId: chainId,
    }).then(res => {
      setContractVersions(res.Data.Versions.map(item => ({
        value: item,
        text: item,
      })));
    });
  }, [chainId, contractAddrWatch, setValue]);

  useEffect(() => {
    GetCompilerVersions().then(res => {
      setCompilerVersions(res.Data.Versions.map(item => ({
        value: item,
        text: item,
      })));
    });
    GetOpenLicenseTypes().then(res => {
      setLicenseOptions(res.Data.LicenseTypes.map(item => ({
        value: item,
        text: item,
      })));
    });
    GetEvmVersions().then(res => {
      setEvmVersions(res.Data.Versions.map(item => ({
        value: item,
        text: item,
      })));
    });
  }, [chainId, setValue]);

  return <PageBox title={''}>
    <div className={'contract-verify-container'}>
      <h1>
        验证&发布智能合约源码
      </h1>
      <p className={'text-base'}>
        该功能帮助用户更透明地查看并交互已部署到链上的智能合约
      </p>
      <Form layout={'vertical'} className={'contract-verify-form tea-mt-5n'}>
        <Form.Item label="合约地址" required>
          <Controller control={control} rules={{
            required: true,
          }} render={({ field }) => {
            return <Input placeholder={'请输入需要验证的合约地址'} {...field} />;
          }} name={'ContractAddr'}/>
        </Form.Item>
        <Form.Item label={
          <GlossaryGuide title={'程序执行入口'}
            className={'text-break'}
            popover={CompilerPathTip}/>
        } required>
          <Controller control={control} rules={{
            required: true,
          }} render={({ field }) => {
            return <Input placeholder={'contract/MainContract.sol:MainContract'} {...field} />;
          }} name={'CompilerPath'}/>
        </Form.Item>
        <Form.Item label="合约版本" required>
          <Controller control={control} rules={{
            required: true,
          }} render={({ field }) => {
            return <Select matchButtonWidth searchable appearence={'button'} size={'l'}
              options={contractVersions}  {...field} />;
          }} name={'ContractVersion'}/>
        </Form.Item>
        <Form.Item label="编译器版本" required>
          <Controller control={control} render={({ field }) => {
            return <Select matchButtonWidth appearence={'button'} size={'l'} options={compilerVersions} {...field}
              searchable/>;
          }} name={'CompilerVersion'}/>
        </Form.Item>
        <Form.Item label="开源许可类型" required>
          <Controller control={control} render={({ field }) => {
            return <Select matchButtonWidth appearence={'button'} size={'l'} options={licenseOptions} {...field}
              searchable/>;
          }} name={'OpenLicenseType'}/>
        </Form.Item>
        <Form.Item>
        </Form.Item>
        <Form.Item
          required
          label="上传合约源码">
          <Controller control={control} rules={{
            required: true,
          }} render={({ field }) => {
            return <FileUpload
              maxSize={1024 * 1024 * 10}
              sumMaxSize={1024 * 1024 * 10}
              className={'contract-verify-upload'}
              noMultiAccept={['.zip']}
              accept={['.zip', '.sol']}
              useReader={false}
              placeholder={'支持上传单个压缩包（.zip）、多个.sol文件，大小限制为10MB'}
              multiple
              {...field}
            />;
          }} name={'ContractSourceFile'}/>
        </Form.Item>
      </Form>
    </div>
    <div className={'contract-verify-container tea-mt-5n'}>
      <h4 className={'advanced-title'}>
        高级设置
      </h4>
      <Form layout={'vertical'} className={'contract-verify-form tea-mt-2n'}>
        <Form.Item label="Optimization">
          <Controller control={control} render={({ field }) => {

            return <Select matchButtonWidth appearence={'button'} size={'l'}
              options={optimization_options} {...field} />;
          }} name={'Optimization'}/>
        </Form.Item>
        <Form.Item label="Runs(Optimizier)">
          <Controller control={control} render={({ field }) => {
            return <Input {...field} />;
          }} name={'Runs'}/>
        </Form.Item>
        <Form.Item label="EVM Version">
          <Controller control={control} render={({ field }) => {
            return <Select matchButtonWidth appearence={'button'} size={'l'} options={evmVersions} clearable {...field}
              searchable/>;
          }} name={'EvmVersion'}/>
        </Form.Item>
      </Form>
    </div>
    <div className={'tea-mt-5n tea-text-center'}>
      <Button type="primary" className={'tea-mr-2n'} onClick={handleSubmit} disabled={!isValid}
        loading={loading}>提交</Button>
      <Button onClick={handleReset}>重置</Button>
    </div>
  </PageBox>;
}


export function ContractVerifiedBadge() {
  return <>
    <Icon type="success" className={'tea-ml-1n'}/>
  </>;
}

