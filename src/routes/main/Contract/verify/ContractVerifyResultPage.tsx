import React, { useCallback, useEffect } from 'react';
import PageBox from '@components/ui/PageBox';
import { Bubble, Copy, Form, Icon, Justify } from 'tea-component';
import { GetContractCodeRes } from '@src/models';
import { useCurrentChainId, useGetContractCode } from '@utils/hooks';
import { jsonUtils, useQuery } from '@utils/utils';
import classNames from 'classnames';
import { Nullable } from '@src/types';
import { monaco_options } from '@src/constant';

import { IStandaloneCodeEditor } from '@utils/editor';

const default_contract_code_height = 200;
const default_contract_abi_height = 100;
/**
 * 合约认证结果详情表单
 * @constructor
 */
export function ContractVerifyResultForm({ code }: { code: GetContractCodeRes }) {
  const contractCodeElRefArr = React.useRef<HTMLDivElement[]>([]);
  const contractCodeEditorRefArr = React.useRef<IStandaloneCodeEditor[]>([]);

  const contractABIElRef = React.useRef<HTMLDivElement>(null);
  const contractAbIEditorRef = React.useRef<IStandaloneCodeEditor>(null);

  const contractAbiFormatted = jsonUtils.parse(code?.ContractAbi);
  const toggleFullscreen = useCallback((editor: Nullable<IStandaloneCodeEditor>,
    el: Nullable<HTMLDivElement>,
    minHeight: number) => {
    if (editor && el) {
      const contentHeight = editor.getScrollHeight();
      if (el.style.height === `${contentHeight}px`) {
        el.style.height = `${minHeight}px`;
      } else {
        el.style.height = `${contentHeight}px`;
      }
    }
  }, []);
  useEffect(() => {
    if (!code) {
      return;
    }
    code.SourceCodes.forEach((sourceCode, index) => {
      if (contractCodeElRefArr.current[index]) {
        const editor = window.monaco.editor.create(contractCodeElRefArr.current[index], {
          ...monaco_options,
          language: 'sol',
        });
        editor.setValue(sourceCode.SourceCode);
        contractCodeEditorRefArr.current[index] = editor;
      }
    });
    if (contractABIElRef.current) {
      const editor = window.monaco.editor.create(contractABIElRef.current, {
        ...monaco_options,
        language: 'json',
        wordWrap: 'on',
      });
      // @ts-ignore
      contractAbIEditorRef.current = editor;
      editor.setValue(contractAbiFormatted);
      setTimeout(() => {
        // contractABIElRef.current.style.height = `${Math.min(editor.getContentHeight(), 100)}px`; // 默认高度至少为100
        editor.getAction('editor.action.formatDocument')?.run();
      }, 300);
    }
  }, [code, contractAbiFormatted]);

  return <>
    <Form className={'display-flex contract-verify-result-form-detail tea-mt-2n'}>
      <Form.Item label={'编译器版本'}>
        <Form.Text>
          {
            code.CompilerVersion
          }
        </Form.Text>
      </Form.Item>
      <Form.Item label={'开源许可类型'}>
        <Form.Text>
          {
            code.OpenLicenseType
          }
        </Form.Text>
      </Form.Item>
      <Form.Item label={'Optimization'}>
        <Form.Text>
          {
            String(code.Optimization)
          }
        </Form.Text>
      </Form.Item>

      <Form.Item label={'Runs(Optimizier)'}>
        <Form.Text>
          {
            String(code.Runs)
          }
        </Form.Text>
      </Form.Item>
      <Form.Item label={'EVM Version'}>
        <Form.Text>
          {
            code.EvmVersion
          }
        </Form.Text>
      </Form.Item>
    </Form>
    <div className={'tea-mt-3n'}/>
    <Form.Title>
      Contract Source Code
    </Form.Title>
    <div className={'contract-verify-editor-form-container te'}>
      {
        code.SourceCodes.map((item, idx) => {
          return <div key={item.SourcePath} className={classNames({
            'tea-mt-5n': idx !== 0,
          })}>
            <Justify left={<Form.Text>
              {item.SourcePath}
            </Form.Text>
            } right={<>
              <Copy text={item.SourceCode}/>
              <Bubble content={'全屏'}>
                <Icon type="fullscreen" className={'tea-ml-1n explorer-btn'} onClick={() => {
                  toggleFullscreen(contractCodeEditorRefArr.current[idx], contractCodeElRefArr.current[idx], default_contract_code_height);
                }}/>
              </Bubble>
            </>}/>
            {
              <div ref={(el) => {
                // @ts-ignore
                contractCodeElRefArr.current[idx] = el;
              }} className={'contract-verify-editor'} style={{
                height: default_contract_code_height,
              }}/>
            }
          </div>;
        })
      }
    </div>
    <div className={'contract-verify-abi tea-mt-5n'}>
      <Form.Title>
        Contract ABI
      </Form.Title>
      <Justify right={<>
        <Copy text={contractAbiFormatted}/>
        <Bubble content={'全屏'}>
          <Icon type="fullscreen" className={'tea-ml-1n explorer-btn'} onClick={() => {
            toggleFullscreen(contractAbIEditorRef.current, contractABIElRef.current, default_contract_abi_height);
          }}/>
        </Bubble>
      </>}/>
      <div ref={contractABIElRef} className={'contract-verify-editor'} style={{
        height: default_contract_abi_height,
      }}/>
    </div>
  </>;
}

/**
 * 合约认证结果详情页
 */
export function ContractVerifyResultPage() {
  const { fetch, code } = useGetContractCode();
  const queryParams = useQuery();
  const chainId = useCurrentChainId();
  useEffect(() => {
    if (!queryParams.get('contractAddr')
      || !queryParams.get('contractVersion')) {
      return;
    }
    fetch({
      chainId,
      contractAddr: queryParams.get('contractAddr')!,
      contractVersion: queryParams.get('contractVersion')!,
    });
  }, [chainId, fetch]);
  return <PageBox title="验证结果">
    {
      code && <ContractVerifyResultForm code={code}/>
    }
  </PageBox>;
}
