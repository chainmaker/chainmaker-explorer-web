import React, { useCallback, useContext, useImperativeHandle } from 'react';
import { Button, Form, Input, Modal, TabPanel, Tabs } from 'tea-component';
import { Controller, useForm } from 'react-hook-form';
import { usePluginLogin } from '@src/routes/main/DBQuery/hooks';
import { ConfirmModalHandle } from '@components/modal';
import { useNavigate } from 'react-router-dom';
import { AppContext } from '@components/context';
import { AccountLogin } from '@utils/apis';
import CryptoJS from 'crypto-js';

/**
 * 提供2种封路方式
 * 1. 密码（用户开启了密码登录设置的话）
 * 2. 插件
 * @constructor
 */
function LoginContainer({
  confirmModalRef,
}: {
  confirmModalRef: React.Ref<ConfirmModalHandle>,
}, ref: React.Ref<any>) {
  const { serverConfig, setAccount } = useContext(AppContext);
  const navigate = useNavigate();
  const { handleLoginClick } = usePluginLogin(confirmModalRef, () => {
    navigate(0);
  });
  const [confirmVisible, setConfirmVisible] = React.useState(false);
  const tabs = [
    { id: 'password', label: '密码登录' },
    { id: 'plugin', label: '插件登录' },
  ];
  const {
    control,
    getValues,
    formState: { isValid },
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      password: '',
    },
  });

  const handlePasswordLogin = useCallback(() => {
    const randomNum = Date.now();
    const encrypted = `${CryptoJS.MD5(`${getValues('password')}_${randomNum}`)}`;
    AccountLogin({
      password: encrypted,
      randomNum: randomNum,
    }).then(res => {
      setConfirmVisible(false);
      setAccount({
        token: res.data.token,
        address: res.data.userAddr,
        name: 'default user',
      });
      navigate(0);
    });
  }, [getValues, navigate, setAccount]);

  useImperativeHandle(ref, () => {
    return {
      login: () => {
        // 如果密码支持的话，则开启二选一，如果不支持，则直接触发插件登录即可。
        if (serverConfig.isSupportAccount) {
          setConfirmVisible(true);
        } else {
          handleLoginClick();
        }
      },
      hide: () => {
        setConfirmVisible(false);
      },
    };
  }, [handleLoginClick, serverConfig.isSupportAccount]);

  return <Modal
    size={'s'}
    visible={confirmVisible}
    onClose={() => {
      setConfirmVisible(false);
    }}
    caption={'登录'}>
    <Modal.Body>
      <Tabs tabs={tabs} placement={'top'} defaultActiveId={'password'} onActive={(tab) => {
        if (tab.id === 'plugin') {
          handleLoginClick();
        }
      }}>
        <TabPanel id="password">
          <Form hideLabel={false} fixedLabelWidth={150} layout={'default'}>
            <Form.Item label="密码">
              <Controller control={control} render={({ field }) => <Input.Password rules={false}{...field}/>} name={'password'}
                rules={{
                  required: true,
                }}/>
            </Form.Item>
            <Button type={'primary'} disabled={!isValid} onClick={handlePasswordLogin}>登录</Button>
          </Form>
        </TabPanel>
        <TabPanel id="plugin"></TabPanel>
      </Tabs>
    </Modal.Body>
  </Modal>;
}

const Login = React.forwardRef(LoginContainer);

export default Login;
