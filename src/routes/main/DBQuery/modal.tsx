import React, { useCallback, useImperativeHandle, useState } from 'react';
import { Controller, useForm } from 'react-hook-form';
import { Button, Form, Input, Modal, Radio } from 'tea-component';
import { getStatus } from '@src/utils/form';
import { SaveRegularQueryTask } from '@src/utils/apis';

function SaveSQLModalContainer({}, ref: any) {
  const {
    control,
    getValues,
    formState: { isValidating, isSubmitted, isValid },
  } = useForm({
    mode: 'onChange',
    defaultValues: {
      isPublic: 'false',
      title: '',
    },
  });
  const [visible, setVisible] = useState(false);
  const onSubmit = useCallback(async () => {
    const values = getValues();
    console.log(values);
    await SaveRegularQueryTask({
      queryId: 0,
      chainId: '',
      title: values.title,
      isPublic: values.isPublic === 'true',
      runType: 0,
      params: [],
    });
    setVisible(false);
  }, [getValues]);

  const onClose = useCallback(() => {
    setVisible(false);
  }, []);

  useImperativeHandle(ref, () => ({
    show: () => {
      setVisible(true);
    },
  }));

  return <Modal caption={'保存'} visible={visible}>
    <Modal.Body>
      <Form
        layout="vertical"
      >
        <Controller
          control={control}
          rules={{
            required: '请输入查询表名称',
          }}
          name="title"
          render={({ field, fieldState }) => (
            <Form.Item
              required
              label={'查询表名称'}
              status={getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              message={fieldState.error?.message}
            >
              <Input {...field} placeholder="请输入查询表名称"/>
            </Form.Item>)}
        />
        <Controller
          control={control}
          rules={{
            required: '请输入查询表名称',
          }}
          name="isPublic"
          render={({ field, fieldState }) => (
            <Form.Item
              required
              label={'是否公开'}
              status={getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              message={fieldState.error?.message}
            >
              <Radio.Group {...field}>
                <Radio name="true">公开</Radio>
                <Radio name="false">不公开</Radio>
              </Radio.Group>
            </Form.Item>)}
        />
      </Form>
    </Modal.Body>
    <Modal.Footer>
      <Button type="weak" onClick={onClose}>
        {'取消'}
      </Button>
      <Button type="primary" onClick={onSubmit} disabled={!isValid}>
        {'确定'}
      </Button>
    </Modal.Footer>
  </Modal>;
}

export const SaveSQLModal = React.forwardRef(SaveSQLModalContainer);


