import React, { useCallback, useContext, useMemo, useState } from 'react';
import style from './index.module.scss';
import Statistics from './components/Statistics/index';
import NewBlocks from './components/NewBlocks/index';
import Chart from './components/Chart/index';
import Deals from './components/Deals/index';
import Contract from './components/Contract/index';
import { Search } from '@src/utils/apis';
import { useNavigate } from 'react-router-dom';
import { notification, Select } from 'tea-component';
import { RootReducer } from '@src/store';
import { useSelector } from 'react-redux';
import { ContractTypeMap } from '@src/constant';
import { AppContext } from '@components/context';

const SearchTypeList = [
  {
    value: '1',
    text: '区块hash',
  },
  {
    value: '2',
    text: '区块高度',
  },
  {
    value: '3',
    text: '交易ID',
  },
  {
    value: '4',
    text: '合约名称',
  },
  {
    value: '5',
    text: '合约地址',
  },
  {
    value: '6',
    text: '账户地址',
  },
  {
    value: '7',
    text: 'BNS',
  },
];

export default function Home() {
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const { appConfig } = useContext(AppContext);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const navigate = useNavigate();
  const handleSQLQueryClick = useCallback(() => {
    navigate(`/${chainId}/dbQuery`);
  }, [chainId, navigate]);
  const [value, setValue] = useState('');
  const [type, setType] = useState('1');
  const onSearch = useCallback(() => {
    const id = value.trim();
    setValue(id);
    Search({
      Value: id,
      ChainId: chainId,
      Type: type,
    }).then((res) => {
      if (res.Data?.Type !== undefined) {
        switch (res.Data.Type) {
          case 0:
            navigate(`/${res.Data.ChainId}/block/${res.Data.Data}`);
            break;
          case 1:
            navigate(`/${res.Data.ChainId}/transaction/${res.Data.Data}`);
            break;
          case 2:
            navigate(`/${res.Data.ChainId}/contract/${res.Data.Data}?ctype=${ContractTypeMap?.[res.Data?.ContractType]}`);
            break;
          case 3:
            navigate(`/${res.Data.ChainId}/chainaccount/deal?accountaddress=${res.Data.Data}`);
            break;
          default:
            notification.error({
              title: '没有搜索到任何结果',
              description: `搜索： ${value}没有任何结果`,
              unique: true,
              duration: 3000,
            });
            break;
        }
      }
    });
  }, [value, chainId, type, navigate]);
  const onKeyPress = useCallback(
    (e) => {
      if (e.which === 13) {
        onSearch();
        e.stopPropagation();
        e.preventDefault();
      }
    },
    [onSearch],
  );
  return (
    <>
      <div className={style.top}>
        <div className={style.top_info} style={{
          width: appConfig.APP_IS_SHOW_DB_QUERY ? undefined : '925px',
        }}>
          <Select
            searchable
            appearance="button"
            className="tea_cover_top_info_select"
            matchButtonWidth
            options={SearchTypeList}
            value={type}
            onChange={value => setType(value)}
            placeholder="请选择搜索类型"
          />
          <div className={style.top_info_search_input}>
            <input
              value={value}
              onChange={(input) => {
                setValue(input.target.value.trim());
              }}
              onKeyPress={onKeyPress}
              placeholder="请输入区块哈希  / 区块高度 / 交易id / 合约名称搜索"
            />
          </div>
          <div onClick={onSearch} className={style.top_info_search_bt}>
            搜索
          </div>
          {
            appConfig.APP_IS_SHOW_DB_QUERY && <a className={style.custom_search} onClick={handleSQLQueryClick}>
              自定义查询
            </a>}
        </div>
        <div className={style.statistics}>
          <Statistics/>
          <Chart/>
        </div>
      </div>
      <div className={style.new_block}>
        <NewBlocks/>
      </div>
      <div className={style.news}>
        <Deals/>
        <Contract/>
      </div>
    </>
  );
}
