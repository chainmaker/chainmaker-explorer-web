import React, { Suspense, useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { Routes, Route, Navigate, useNavigate, Link, useLocation } from 'react-router-dom';
import style from './index.module.scss';
import ChainSelect from './components/ChainSelect';
import ImgFlower from '@imgs/flower.png';
import ImgChainMaker from '@imgs/chainmaker.png';
import TWEEN from '@tweenjs/tween.js';
import { useSelector } from 'react-redux';
import { RootReducer } from '@src/store';
import { GetChainConfig, GetMainCrossConfig } from '@src/utils/apis';
import { AppContext } from '@components/context';

const Home = React.lazy(() => import('./Home'));
const Chain = React.lazy(() => import('./Chain'));
const Block = React.lazy(() => import('./Block'));
const SubChain = React.lazy(() => import('./SubChain'));
const BlockDetail = React.lazy(() => import('./Block/Detail'));
const Transaction = React.lazy(() => import('./Transaction'));
const TransactionDetail = React.lazy(() => import('./Transaction/Detail'));
const Contract = React.lazy(() => import('./Contract'));
const ContractDetail = React.lazy(() => import('./Contract/Detail'));
const ChainAccount = React.lazy(() => import('./ChainAccount'));
const Node = React.lazy(() => import('./Node'));
const Origin = React.lazy(() => import('./Origin'));
const User = React.lazy(() => import('./User'));
const NFTDetail = React.lazy(() => import('./Transaction/NFTDetail'));
const CrossTransactionDetail = React.lazy(() => import('./SubChain/transactionDetail').then(m => ({ default: m.CrossTransactionDetail })));
const SubChainDetail = React.lazy(() => import('./SubChain/subchainDetail').then(m => ({ default: m.SubChainDetail })));
const SubChainList = React.lazy(() => import('./SubChain/subchainList').then(m => ({ default: m.SubChainList })));
const CrossChainList = React.lazy(() => import('./SubChain/crossChainList').then(m => ({ default: m.CrossChainList })));
const DBQueryList = React.lazy(() => import('./DBQuery'));
const DBQueryCreate = React.lazy(() => import('./DBQuery/detail'));
const IdaDataDetail = React.lazy(() => import('./ida/ida-data-detail'));
const ContractVerify = React.lazy(() => import('./Contract/verify/ContractVerify').then(m => ({ default: m.ContractVerify })));
const ContractVerifyResultPage = React.lazy(() => import('./Contract/verify/ContractVerifyResultPage').then(m => ({ default: m.ContractVerifyResultPage })));

function animate(time: any) {
  requestAnimationFrame(animate);
  TWEEN.update(time);
}

requestAnimationFrame(animate);

export default function Main() {
  const { appConfig } = useContext(AppContext);
  const navigate = useNavigate();
  const location = useLocation();
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const [hasChainSetting, setHasChainSeeting] = useState<boolean>(false);
  const [isShowSubChain, setIsShowSubChain] = useState<boolean>(false);
  // const dispatch = useDispatch();
  // const trustedChainId = useSelector((state: RootReducer) => state.trustedChainIdReducer);
  const updateChainId = useCallback((value) => {
    window.open(`/${value}/home`);
  }, []);
  const onInit = useCallback((value) => {
    setTimeout(() => {
      navigate(`/${value}/home`, { replace: true });
    }, 0);
  }, []);
  /**
   * 获取平台唯一的链Id，防篡改链Id
   */

  useEffect(() => {
    GetChainConfig({}).then((res) => {
      setHasChainSeeting(res.Data);
    });
    // setHasChainSeeting(true);
  }, []);

  useEffect(() => {
    if (chainId) {
      GetMainCrossConfig({ ChainId: chainId }).then((res) => {
        setIsShowSubChain(res.Data.ShowTag);
      });
    }
  }, [chainId]);
  const [showTitle, setShowTitle] = useState(true);

  useEffect(() => {
    console.log(location);
    setShowTitle(location.pathname === `/${chainId}/home` || location.pathname === `/${chainId}/subchain`);

  }, [location.pathname, chainId]);

  return (
    <>
      <div className={style.header}>
        <div className={style.header_c}>
          <Link to={`/${chainId}/home`}>
            <img src={ImgChainMaker} className={style.header_logo}/>
          </Link>
          {showTitle && isShowSubChain && <>
            <Link to={`/${chainId}/home`}
              className={`${style.header_tab} ${location.pathname === ('/' + chainId + '/home') ? style.active_tab : ''}`}>浏览器</Link>
            <Link to={`/${chainId}/subchain`}
              className={`${style.header_tab} ${location.pathname === ('/' + chainId + '/subchain') ? style.active_tab : ''}`}>主子链网</Link>
          </>}
          <div className={style.header_setting}>
            <ChainSelect onChange={updateChainId} onInit={onInit}/>
            {hasChainSetting && (
              <img
                onClick={() => navigate(`/${chainId}/chain`)}
                src={ImgFlower}
                className={style.header_seeting_icon}
              />
            )}
          </div>
        </div>
      </div>
      {chainId && chainId !== 'v' && (
        <Suspense fallback={null}>
          <Routes>
            <Route path="home" element={<Home/>}/>
            <Route path="node" element={<Node/>}/>
            <Route path="chain" element={<Chain/>}/>
            <Route path="subchain/crossTransactionDetail" element={<CrossTransactionDetail/>}/>
            <Route path="subchain/subChainDetail" element={<SubChainDetail/>}/>
            <Route path="subchain/subChainList" element={<SubChainList/>}/>
            <Route path="subchain/crossChainList" element={<CrossChainList/>}/>
            <Route path="subchain" element={<SubChain/>}/>
            <Route path="block" element={<Block/>}/>
            <Route path="block/:blockHash" element={<BlockDetail/>}/>
            <Route path="transaction" element={<Transaction/>}/>
            <Route path="transaction/:txId" element={<TransactionDetail/>}/>
            <Route path="nft" element={<NFTDetail/>}/>
            <Route path="chainaccount/*" element={<ChainAccount/>}/>
            <Route path="contract" element={<Contract/>}/>
            <Route path="contract/verify" element={<ContractVerify/>}/>
            <Route path="contract/verify/result" element={<ContractVerifyResultPage/>}/>
            <Route path="contract/ida/:contractAddr" element={<IdaDataDetail/>}/>
            <Route path="contract/:contractAddr/*" element={<ContractDetail/>}/>
            <Route path="origin" element={<Origin/>}/>
            <Route path="user" element={<User/>}/>
            {appConfig.APP_IS_SHOW_DB_QUERY && <>
              <Route path="dbQuery" element={<DBQueryList/>}/>
              <Route path="dbQuery/new" element={<DBQueryCreate/>}/>
              <Route path="dbQuery/:taskId" element={<DBQueryCreate/>}/>
            </>}
            <Route path="*" element={<Navigate to="home"/>}/>
          </Routes>
        </Suspense>
      )}
    </>
  );
}
