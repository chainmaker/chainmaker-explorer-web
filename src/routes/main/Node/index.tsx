import React, { useCallback, useEffect, useMemo, useState } from 'react';
import liststyle from '../list.module.scss';
import PageBox from '@src/components/ui/PageBox';
import { Button, Form, Input, Justify, Select, Table, TableColumn } from 'tea-component';
import { NodeItem } from '@src/models';
import { GetNodeList, GetOrgList } from '@src/utils/apis';
import { formatAddress } from '@src/utils/tools';
import { RootReducer } from '@src/store';
import { useDispatch, useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

const { pageable, autotip } = Table.addons;

export default function Chain() {
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const authType = useMemo(() => currentChain?.AuthType, [currentChain]);
  const [options, setOptions] = useState<any[]>([]);
  // const [nodeName, setNodeName] = useState('');
  // const [nodeId, setNodeId] = useState('');
  // const [orgId, setOrgId] = useState('');
  // const [pageIndex, setPageIndex] = useState<number>(1);
  // const [pageSize, setPageSize] = useState<number>(10);
  const { nodeName, nodeId, orgId, pageIndex, pageSize } = useSelector(
    (state: RootReducer) => state.nodeListParamReducer,
  );
  const dispatch = useDispatch();
  const [recordCount, setRecordCount] = useState<number>(0);
  const [isLoading, setIsLoading] = useState(false);
  const [list, setList] = useState<NodeItem[]>([]);
  const updateparam = useCallback((payload) => {
    dispatch({
      type: 'UPDATE_NODELIST_PARAM',
      payload,
    });
  }, []);
  const getList = useCallback(() => {
    setIsLoading(true);
    GetNodeList({
      ChainId: chainId,
      Offset: pageIndex - 1,
      Limit: pageSize,
      NodeName: nodeName,
      NodeId: nodeId,
      OrgId: orgId,
    })
      .then((res) => {
        setIsLoading(false);
        if (res.GroupList) {
          setList(res.GroupList);
          setRecordCount(res.TotalCount);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  }, [chainId, orgId, nodeId, nodeName, pageIndex, pageSize]);
  useEffect(() => {
    getList();
  }, [pageIndex, chainId, pageSize]);
  useEffect(() => {
    if (authType !== 'permissionedwithcert') {
      return;
    }
    GetOrgList({
      ChainId: chainId,
      Offset: 0,
      Limit: 9999,
    }).then((res) => {
      if (res.GroupList) {
        setOptions(
          res.GroupList.map((item) => ({
            value: item.OrgId,
            text: item.OrgId,
          })),
        );
      }
    });
  }, []);
  const columns = useMemo<TableColumn[]>(() => {
    const list: TableColumn<NodeItem>[] = [
      {
        key: 'NodeId',
        header: '节点Id',
        align: 'left',
        render: ({ NodeId }) => {
          return <Link to={`/${chainId}/block?nodeId=${NodeId}`}>{NodeId}</Link>;
        },
      },
      {
        key: 'NodeAddress',
        header: '节点IP和端口',
        align: 'left',
        render: ({ NodeAddress }) => formatAddress(NodeAddress),
      },
      {
        key: 'Role',
        header: '节点角色',
        align: 'left',
        width: 160,
      },
    ];
    if (authType === 'permissionedwithcert') {
      list.splice(1, 0, {
        key: 'NodeName',
        header: '节点名称',
        align: 'left',
      });
      list.push({
        key: 'OrgId',
        header: '所属组织',
        align: 'left',
      });
    }
    return list;
  }, []);

  const onSearch = useCallback(() => {
    updateparam({ pageIndex: 1 });
    getList();
  }, [getList]);
  return (
    <PageBox title="节点列表">
      <Justify
        right={
          <>
            <Form className={liststyle.searchform} hideLabel={false} layout="inline">
              {authType === 'permissionedwithcert' && (
                <Form.Item label="所属组织">
                  <Select
                    options={options}
                    clearable
                    matchButtonWidth
                    appearance="button"
                    size="m"
                    onChange={(value) => updateparam({ orgId: value })}
                    value={orgId}
                  />
                </Form.Item>
              )}
              {authType === 'permissionedwithcert' && (
                <Form.Item label="节点名称">
                  <Input
                    onChange={(value) => updateparam({ nodeName: value })}
                    value={nodeName}
                    placeholder="请输入请输入节点名称"
                  />
                </Form.Item>
              )}
              <Form.Item label="节点Id">
                <Input
                  onChange={(value) => updateparam({ nodeId: value })}
                  value={nodeId}
                  placeholder="请输入节点Id搜索"
                />
              </Form.Item>
              <Form.Item label="">
                <Button type="primary" onClick={onSearch}>
                  搜索
                </Button>
              </Form.Item>
            </Form>
          </>
        }
      />
      <div className={liststyle.table}>
        <Table
          style={{
            marginTop: 24,
          }}
          className={liststyle.table}
          compact={true}
          records={list}
          recordKey="NodeId"
          bordered={true}
          disableTextOverflow={true}
          columns={columns}
          addons={[
            autotip({
              isLoading,
            }),
            pageable({
              recordCount,
              pageIndex,
              onPagingChange: (query) => {
                if (query?.pageIndex) {
                  updateparam({ pageIndex: query.pageIndex });
                }
                if (query?.pageSize) {
                  updateparam({ pageSize: query.pageSize });
                }
              },
            }),
          ]}
        />
      </div>
    </PageBox>
  );
}
