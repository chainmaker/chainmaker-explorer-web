import React, { useCallback, useEffect, useRef, useState } from 'react';
import { DatePicker, Table, TableColumn, Text, Button, Input } from 'tea-component';
const { pageable, autotip } = Table.addons;
import PageBox from '../../../components/ui/PageBox';
import { useNavigate, Link } from 'react-router-dom';
import { GetCrossTxList } from '@src/utils/apis';
import { CrossTxListItem, GetCrossTxListParam } from '@src/models';
import { useParams } from 'react-router-dom';
import { TextTheme } from '@src/constant';
import { formatTime } from '@src/utils/tools';
import style from './index.module.scss';
import { CrossChainLink } from '@src/routes/main/components/CrossChainLink';
import { RootReducer } from '@src/store';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment/moment';

const { RangePicker } = DatePicker;

const CrossTxListStatusMap: { [key: string]: { text: string; theme: TextTheme } } = {
  0: { text: '进行中', theme: 'warning' },
  1: { text: '成功', theme: 'success' },
  2: { text: '失败', theme: 'danger' },
};


export function CrossChainTable(props: {
  SubChainId?: string
}) {
  const navigate = useNavigate();
  const { chainId } = useParams();
  const dispatch = useDispatch();

  const listParamReducer = useSelector((state: RootReducer) => state.crossChainListParamReducer);

  const [data, setData] = useState<{
    total: number;
    list: CrossTxListItem[];
  }>({
    total: 0,
    list: [],
  });

  const updateparam = useCallback((payload) => {
    dispatch({
      type: 'UPDATE_CROSS_CHAIN_LIST_PARAM',
      payload,
    });
  }, [dispatch]);

  const [isLoading, setIsLoading] = useState(false);

  const onSearch = (newPartialParams = {}) => {
    setIsLoading(true);
    const newParams = {
      ...listParamReducer,
      ...newPartialParams,
    };
    updateparam(newPartialParams);
    const params: GetCrossTxListParam = {
      ChainId: chainId,
      Offset: newParams.Offset,
      Limit: newParams.Limit,
      CrossId: newParams.CrossId,
      FromChainName: newParams.FromChainName,
      ToChainName: newParams.ToChainName,
      StartTime: newParams.StartTime ? Math.floor(newParams.StartTime / 1000) : 0,
      EndTime: newParams.EndTime ? Math.ceil(newParams.EndTime / 1000) : 0,
    };

    if (props?.SubChainId) {
      params.SubChainId = props?.SubChainId;
    }

    GetCrossTxList(params).then((res) => {
      if (res.GroupList?.length) {
        setData({
          total: res.TotalCount,
          list: res.GroupList,
        });
      } else {
        setData({ total: 0, list: [] });
      }
    }).catch((e) => {
      console.log(e);
      return;
    }).finally(() => {
      setIsLoading(false);
    });
  };

  const onSearchRef = useRef(onSearch);
  onSearchRef.current = onSearch;

  const columns: TableColumn<CrossTxListItem>[] = [
    {
      key: 'CrossId',
      header: '跨链ID',
      align: 'left',
      width: 100,
      render: (record: CrossTxListItem) => {
        return <Link
          to={`/${chainId}/subchain/crossTransactionDetail?crossId=${record.CrossId}`}>{record.CrossId}</Link>;
      },
    },
    {
      key: 'FromChainName',
      header: '发起链名',
      align: 'left',
      render: (record: CrossTxListItem) => {
        return <CrossChainLink chainId={chainId} subChainId={record.FromChainId} subIsMainChain={record.FromIsMainChain}
          name={record.FromChainName}/>;
      },
    },
    {
      key: 'ToChainName',
      header: '接收链名',
      align: 'left',
      render: (record: CrossTxListItem) => {
        return <CrossChainLink chainId={chainId} subChainId={record.ToChainId} subIsMainChain={record.ToIsMainChain}
          name={record.ToChainName}/>;
      },
    },
    {
      key: 'Status',
      header: '跨链交易状态',
      align: 'left',
      width: 168,
      render: (record: CrossTxListItem) => {
        return <Text
          theme={CrossTxListStatusMap[record?.Status ?? '']?.theme}>{CrossTxListStatusMap[record?.Status ?? '']?.text}</Text>;
      },
    },
    {
      key: 'Timestamp',
      header: '跨链发起时间',
      align: 'left',
      width: 168,
      render: (record: CrossTxListItem) => {
        return formatTime(record.Timestamp * 1000);
      },
    },
    {
      key: 'setting',
      header: '操作',
      align: 'left',
      width: 100,
      render: (record: CrossTxListItem) => {
        return (
          <>
            <Button type="link"
              onClick={() => navigate(`/${chainId}/subchain/crossTransactionDetail?crossId=${record.CrossId}`)}>查看</Button>
          </>
        );
      },
    },
  ];

  useEffect(() => {
    onSearchRef.current();
  }, []);

  return (
    <>
      <div className={style.sub_chain_list_search}>
        <div>
          <Text>起止时间：</Text>
          <RangePicker
            clearable
            onChange={value => {
              updateparam({
                StartTime: value[0] ? value[0].startOf('day').valueOf() : null,
                EndTime: value[1] ? value[1].endOf('day').valueOf() : null,
              });
            }
            }
            defaultValue={
              (listParamReducer.StartTime && listParamReducer.EndTime)
                ? [moment(new Date(listParamReducer.StartTime)), moment(listParamReducer.EndTime)]
                : undefined
            }
          />
        </div>
        <div>
          <Text>跨链ID：</Text>
          <Input
            defaultValue={listParamReducer.CrossId}
            size="m"
            onChange={(value) => {
              updateparam({ CrossId: value });
            }}
            placeholder="请输入跨链ID"
          />
        </div>
        <div>
          <Text>发起链：</Text>
          <Input
            defaultValue={listParamReducer.FromChainName}
            size="m"
            onChange={(value) => {
              updateparam({ FromChainName: value });
            }}
            placeholder="请输入发起方名称"
          />
        </div>
        <div>
          <Text>接收链：</Text>
          <Input
            defaultValue={listParamReducer.ToChainName}
            size="m"
            onChange={(value) => {
              updateparam({ ToChainName: value });
            }}
            placeholder="请输入接收方名称"
          />
        </div>
        <Button
          type="primary"
          onClick={() => {
            onSearchRef.current( {
              Offset: 0,
            });
          }}
        >
          搜索
        </Button>
      </div>
      <Table
        className={style.detail_table}
        records={data.list}
        recordKey="CrossId"
        bordered={true}
        columns={columns}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            recordCount: data.total,
            pageIndex: listParamReducer.Offset! + 1,
            pageSize: listParamReducer.Limit,
            onPagingChange: (query) => {
              onSearchRef.current({
                Offset: query.pageIndex! - 1,
                Limit: query.pageSize,
              });
            },
          }),
        ]}
      />
    </>
  );
}

export function CrossChainList() {
  return (
    <PageBox title="跨链交易列表">
      <CrossChainTable/>
    </PageBox>
  );
}
