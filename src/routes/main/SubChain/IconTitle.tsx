import React from 'react';

export const IconTitle = (title: string) => (
  <div>
    <div style={{ width: '3px', height: '20px', background: '#185CFF', display: 'inline-block', margin: '-5px 5px' }}></div>
    {title}
  </div>
);
