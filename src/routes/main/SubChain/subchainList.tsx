import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Bubble, Button, Input, Table, Text } from 'tea-component';
import PageBox from '../../../components/ui/PageBox';
import { Link, useNavigate, useParams } from 'react-router-dom';
import { CrossSubChainList } from '@src/utils/apis';
import { CrossSubChainListItem, CrossSubChainListParam } from '@src/models';
import { SubChainStatusMap } from '@src/constant';
import style from './index.module.scss';
import { formatTime } from '@src/utils/tools';
import { useDispatch, useSelector } from 'react-redux';
import { RootReducer } from '@src/store';

const { pageable, autotip } = Table.addons;

export function SubChainList() {
  const navigate = useNavigate();
  const { chainId } = useParams();
  const dispatch = useDispatch();
  const listParamReducer = useSelector((state: RootReducer) => state.subChainListParamReducer);
  const [data, setData] = useState<{
    total: number;
    list: CrossSubChainListItem[];
  }>({
    total: 0,
    list: [],
  });
  const updateparam = useCallback((payload:Partial<CrossSubChainListParam>) => {
    dispatch({
      type: 'UPDATE_SUB_CHAIN_LIST_PARAM',
      payload,
    });
  }, [dispatch]);

  const [isLoading, setIsLoading] = useState(false);

  const onSearch = (newPartialParams = {}) => {
    setIsLoading(true);
    const newParams = {
      ...listParamReducer,
      ...newPartialParams,
    };
    updateparam(newPartialParams);
    CrossSubChainList({
      ChainId: chainId,
      Offset: newParams.Offset,
      Limit: newParams.Limit,
      SubChainId: newParams.SubChainId,
      SubChainName: newParams.SubChainName,
    }).then((res) => {
      setData({
        total: res.TotalCount,
        list: res.GroupList,
      });
    }).finally(() => {
      setIsLoading(false);
    });
  };

  const onSearchRef = useRef(onSearch);
  onSearchRef.current = onSearch;

  useEffect(() => {
    onSearchRef.current();
  }, []);

  return (
    <PageBox title="子链列表">
      <div className={style.sub_chain_list_search}>
        <div>
          <Text>子链名称：</Text>
          <Input
            defaultValue={listParamReducer.SubChainName}
            size="l"
            onChange={(value) => {
              updateparam({ SubChainName: value });
            }}
            placeholder="输入子链名称"
          />
        </div>
        <div>
          <Text>链ID：</Text>
          <Input
            defaultValue={listParamReducer.SubChainId}
            size="l"
            onChange={(value) => {
              updateparam({ SubChainId: value });
            }}
            placeholder="请输入链ID"
          />
        </div>
        <Button
          type="primary"
          onClick={() => {
            onSearchRef.current({
              Offset: 0,
            });
          }}
        >搜索
        </Button>
      </div>
      <Table
        className={style.detail_table}
        records={data.list}
        recordKey="SubChainId"
        bordered={true}
        columns={[
          {
            key: 'SubChainName',
            header: '子链名称',
            align: 'left',
            render: (record: CrossSubChainListItem) => {
              return <Link to={`/${chainId}/subchain/subChainDetail?subChainId=${record.SubChainId}`}>{record.SubChainName}</Link>;
            },
          },
          {
            key: 'SubChainId',
            header: 'SubchainID',
            align: 'left',
            render: (record: CrossSubChainListItem) => {
              return <Link to={`/${chainId}/subchain/subChainDetail?subChainId=${record.SubChainId}`}>{record.SubChainId}</Link>;
            },
          },
          {
            key: 'BlockHeight',
            header: '区块高度',
            align: 'left',
          },
          {
            key: 'CrossTxNum',
            header: '累计跨链交易数',
            align: 'left',
          },
          {
            key: 'CrossContractNum',
            header: '累计跨链合约数据',
            align: 'left',
          },
          {
            key: 'Status',
            header: '运行状态',
            align: 'left',
            render: (record: CrossSubChainListItem) => {
              return SubChainStatusMap?.[record.Status].text;
            }
          },
          {
            key: 'Timestamp',
            header: '同步时间',
            align: 'left',
            render: (record) => formatTime(record.Timestamp * 1000),
            width: 200
          },
          {
            key: 'setting',
            header: '操作',
            render: (record: CrossSubChainListItem) => {
              return (
                <>
                  <a target='_blank' onClick={() => navigate(`/${chainId}/subchain/subChainDetail?subChainId=${record.SubChainId}`)}>查看</a>
                  {' '}
                  {!record.ExplorerUrl ? <Bubble content="仅内部访问">浏览器</Bubble> : <a target='_blank' href={record.ExplorerUrl} rel="noreferrer" >浏览器</a>}
                </>
              );
            }
          },
        ]}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            recordCount: data.total,
            pageIndex: listParamReducer.Offset! + 1,
            pageSize: listParamReducer.Limit,
            onPagingChange: (query) => {
              onSearchRef.current({ Offset: query.pageIndex! - 1, Limit: query.pageSize });
            },
          }),
        ]}
      />
    </PageBox>
  );
}
