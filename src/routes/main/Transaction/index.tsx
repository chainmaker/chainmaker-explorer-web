import React, { useCallback, useEffect, useMemo, useState } from 'react';
import liststyle from '../list.module.scss';
import PageBox from '@components/ui/PageBox';
import { Button, Col, Form, Input, Row, Select, Table, TableColumn, Text } from 'tea-component';
import { GetAccountDetail, GetTxList } from '@src/utils/apis';
import { Tx } from '@src/models';
import { formatUnixTime } from '@src/utils/tools';
import { Link } from 'react-router-dom';
import { RangePicker } from 'tea-component/lib/datepicker/RangePicker';
import { useDispatch, useSelector } from 'react-redux';
import moment, { Moment } from 'moment';

import SearchLink from '@src/components/form/SearchLink';
import { RootReducer } from '@src/store';
import { TxStatusMap, TxStatusOptions } from '@src/constant';
import { UserAddress } from '@src/components/ui/AttrShow/UserAddress';

const { pageable, autotip } = Table.addons;

export default function Transaction() {
  const dispatch = useDispatch();
  // const now = new Date().getTime();
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId, [currentChain]);
  const authType = useMemo(() => currentChain?.AuthType, [currentChain]);
  const { value, sender, userAddr, txStatus, starttime, endtime, pageIndex, pageSize, contractName } = useSelector(
    (state: RootReducer) => state.txListParamReducer,
  );
  const updateparam = useCallback((payload) => {
    dispatch({
      type: 'UPDATE_TXLIST_PARAM',
      payload,
    });
  }, []);
  const [list, setList] = useState<Tx[]>([]);
  const [recordCount, setRecordCount] = useState<number>(0);
  const [isLoading, setIsLoading] = useState(false);
  const updateTime = useCallback(
    (value: Moment[]) => {
      updateparam({
        starttime: value[0] ? value[0].startOf('day').toDate().getTime() : null,
        endtime: value[1] ? value[1].endOf('day').toDate().getTime() : null,
      });
    },
    [updateparam],
  );
  const getList = async () => {
    setIsLoading(true);
    const params = {
      ChainId: chainId,
      Limit: pageSize,
      TxId: value,
      UserAddrs: userAddr,
      Offset: pageIndex - 1,
      Senders: sender,
      StartTime: starttime ? Math.floor(starttime / 1000) : 0,
      EndTime: endtime ? Math.ceil(endtime / 1000) : 0,
      TxStatus: txStatus,
      ContractName: contractName,
    };
    if (/cnbn$/.test(userAddr)) {
      const res = await GetAccountDetail({
        BNS: userAddr,
        ChainId: chainId || '',
      });
      if (res?.Data?.Address) {
        params.UserAddrs = res.Data.Address;
      }
    };

    GetTxList(params)
      .then((res) => {
        setIsLoading(false);
        if (res.GroupList) {
          setList(res.GroupList);
          setRecordCount(res.TotalCount);
        }
      })
      .catch(() => {
        setIsLoading(false);
      });
  };
  const columns = useMemo<TableColumn[]>(() => {
    const list: TableColumn<Tx>[] = [
      {
        key: 'BlockHeight',
        header: '区块高度',
        align: 'left',
        width: 100,
        render: ({ BlockHeight, BlockHash }) => <Link to={`/${chainId}/block/${BlockHash}`}>{BlockHeight}</Link>,
      },
      {
        key: 'TxId',
        header: '交易Id',
        align: 'left',
        width: 220,
        render: ({ TxId }) => <Link to={`/${chainId}/transaction/${TxId}`}>{TxId}</Link>,
      },
      // {
      //   key: 'SenderOrgId',
      //   header: '发起组织',
      //   align: 'left',
      // },
      {
        key: 'Sender',
        header: '发起用户',
        align: 'left',
        render: ({ Sender, UserAddr, UserAddrBns }) => <UserAddress UserAddrBns={UserAddrBns} Sender={Sender} UserAddr={UserAddr} />
        // authType === 'permissionedwithcert'
        //   ? ({ Sender, UserAddr }) => <Bubble content={UserAddr}>{Sender}</Bubble>
        //   : ({ UserAddr }) => UserAddr || '--',
      },
      {
        key: 'ContractName',
        header: '目标合约',
        align: 'left',
        render: ({ ContractName, ContractAddr }) => (
          <SearchLink type="5" keyWord={ContractAddr}>
            {ContractName}
          </SearchLink>
        ),
      },
      {
        key: 'TxStatusCode',
        header: '交易状态',
        align: 'left',
        width: 100,
        render: ({ TxStatus }) => (
          <Text theme={TxStatusMap[TxStatus].theme}>{TxStatusMap[TxStatus].text}</Text>
        ),
      },
      {
        key: 'Timestamp',
        header: '上链时间',
        align: 'left',
        render: ({ Timestamp }) => formatUnixTime(Timestamp),
        width: 180,
      },
      {
        key: 'Option',
        header: '操作',
        align: 'center',
        width: 100,
        render: ({ TxId }) => (
          <div className={liststyle.setting_c}>
            <Link to={`/${chainId}/transaction/${TxId}`}>查看</Link>
          </div>
        ),
      },
    ];
    if (authType === 'permissionedwithcert') {
      list.splice(2, 0, {
        key: 'SenderOrgId',
        header: '发起组织',
        align: 'left',
      });
    }
    return list;
  }, []);
  useEffect(() => {
    getList();
  }, [chainId, pageIndex, pageSize]);
  const onSearch = () => {
    updateparam({ pageIndex: 1 });
    getList();
  };
  return (
    <PageBox title="交易列表">
      <Row>
        <Col>
          <Form className={liststyle.searchFormFilter} hideLabel={false} layout="inline">
            <Form.Item label="起止时间">
              <RangePicker onChange={updateTime} showTime={false} clearable value={
                (starttime && endtime)
                  ? [moment(new Date(starttime)), moment(endtime)]
                  : undefined
              }/>
            </Form.Item>
            <Form.Item label="合约名称">
              <Input onChange={(value) => updateparam({ contractName: value })} value={contractName}
                placeholder="请输入合约名称搜索"/>
            </Form.Item>
            {authType === 'permissionedwithcert' && (
              <Form.Item label="用户名">
                <Input
                  onChange={(sender) => updateparam({ sender })}
                  value={sender}
                  placeholder="请输入发起用户名"
                />
              </Form.Item>
            )}
            <Form.Item label="交易Id">
              <Input onChange={(value) => updateparam({ value })} value={value} placeholder="请输入交易Id搜索"/>
            </Form.Item>
            <Form.Item label="交易状态">
              <Select
                options={TxStatusOptions}
                onChange={(txStatus) => updateparam({ txStatus })}
                clearable
                matchButtonWidth
                appearance="button"
                size="m"
                value={String(txStatus)}
              />
            </Form.Item>
            <Form.Item label="账户地址">
              <Input
                onChange={(userAddr) => updateparam({ userAddr })}
                value={userAddr}
                // placeholder="请输入用户地址"
                placeholder={authType === 'permissionedwithcert' ? '请输入发起账户地址/链账户名称' : '请输入发起账户地址/BNS'}
              />
            </Form.Item>
          </Form>
          <Button className={'cm-btn'} type="primary" onClick={onSearch}>
            搜索
          </Button>
        </Col>
      </Row>
      <Table
        className={liststyle.table}
        compact={false}
        records={list}
        recordKey="TxId"
        bordered={true}
        disableTextOverflow={true}
        columns={columns}
        addons={[
          autotip({
            isLoading,
          }),
          pageable({
            recordCount,
            pageIndex,
            pageSize,
            onPagingChange: (query) => {
              if (query?.pageIndex) {
                updateparam({ pageIndex: query.pageIndex });
              }
              if (query?.pageSize) {
                updateparam({ pageSize: query.pageSize });
              }
            },
          }),
        ]}
      />
    </PageBox>
  );
}
