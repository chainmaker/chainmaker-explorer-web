import type Monaco from 'monaco-editor';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: any;
    chainMaker: any;
    monaco: Monaco;
  }

  /**
   * @description 具体配置值见Package.json
   */
  const CHAIN_MAKER: {
    version: string; // 版本号,比如 1.0.0
  };
}

export {};
