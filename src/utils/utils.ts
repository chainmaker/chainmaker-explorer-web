export function useQuery() {
  return new URLSearchParams(location.search);
}

export function keyBy<T>(list: T[], key: string) {
  return list.reduce((acc, item: T) => {
    // @ts-ignore
    acc[item[key]] = item;
    return acc;
  }, {} as { [key: string]: T });
}

export function joinBy(splitter: string, ...list: string[]) {
  return list.filter(item => item).join(splitter);
}

export function distinct<T>(arr: T[]) {
  return Array.from(new Set(arr));
}

export const jsonUtils = {
  parse: (str: string) => {
    try {
      return JSON.parse(str);
    } catch {
      return str;
    }
  },
};

export const qsUtils = {
  stringify: (obj: {}) => {
    return Object.entries(obj).reduce((acc, [key, val]) => {
      acc.push(`${key}=${val}`);
      return acc;
    }, [] as string[]).join('&');
  },
};

export function saveAs(blob: Blob, filename: string) {
  const a = document.createElement('a');
  a.href = URL.createObjectURL(blob);
  a.download = filename;
  a.click();
  URL.revokeObjectURL(a.href);
}

/**
 * semver版本比较
 * // if (result > 0) {
 * //   console.log(`${version1} is greater than ${version2}`);
 * // } else if (result < 0) {
 * //   console.log(`${version1} is less than ${version2}`);
 * // } else {
 * //   console.log(`${version1} is equal to ${version2}`);
 * // }
 */
export function versionCompare(version1: string, version2: string) {
  const regex = /(\d+)/g;
  const v1 = version1.match(regex)?.map(Number);
  const v2 = version2.match(regex)?.map(Number);
  if (!v1 || !v2) {
    return 0;
  }
  for (let i = 0; i < v1.length || i < v2.length; i++) {
    const num1 = v1[i] || 0;
    const num2 = v2[i] || 0;
    if (num1 > num2) {
      return 1;
    }
    if (num1 < num2) {
      return -1;
    }
  }
  return 0;
}
