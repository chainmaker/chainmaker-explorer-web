import { ResponseData, ResponseIf, ResponseList } from './apis/type';
/**
 * KEY 接口名称
 * VALUE 接口返回值
 */
const mock: { [propName: string]: ResponseIf<ResponseData<any> | ResponseList<any>> } = {
  Decimal: {
    Response: {
      Data: {
        BlockHeight: 26,
        TransactionNum: 100,
        ContractCount: 12,
        OrgCount: 2,
        NodeCount: 26,
        UserNum: 12,
      },
    },
  },
  GetTransactionNumByTime: {
    Response: {
      TotalCount: 1,
      GroupList: [
        {
          TxNum: 134,
          Timestamp: 1601619261000,
        },
        {
          TxNum: 434,
          Timestamp: 1602629261000,
        },
        {
          TxNum: 543,
          Timestamp: 1603639261000,
        },
        {
          TxNum: 877,
          Timestamp: 1604649261000,
        },
        {
          TxNum: 455,
          Timestamp: 1605659261000,
        },
        {
          TxNum: 777,
          Timestamp: 1606669261000,
        },
      ],
    },
  },
  GetOrgList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          OrgId: 'org1.chainmaker.com',
          NodeCount: 2,
          UserCount: 3,
          Timestamp: 1606669261,
        },
        {
          Id: 1,
          OrgId: 'org1.chainmaker.com',
          NodeCount: 1,
          UserCount: 3,
          Timestamp: 1606669261,
        },
      ],
    },
  },
  GetUserList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          UserId: 'sdasdb12d',
          Role: 'admin',
          OrgId: 'xxx',
          Timestamp: 1606669261,
        },
        {
          Id: 2,
          UserId: 'sdasdb12d',
          Role: 'user',
          OrgId: 'SUCCESS',
          Timestamp: 1606669261,
        },
      ],
    },
  },
  GetNodeList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          NodeId: 'xxxx',
          NodeName: 'node1',
          NodeAddress: '/ip4/127.0.0.1/tcp/11303',
          Role: 'common',
          OrgId: 'org1',
          BlockHeight: 1232,
        },
        {
          Id: 2,
          NodeId: 'xxxx',
          NodeName: 'node1',
          NodeAddress: '/ip4/127.0.0.1/tcp/11303',
          Role: 'common',
          OrgId: 'org1',
          BlockHeight: 1232,
        },
      ],
    },
  },
  GetBlockList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          BlockHeight: 102,
          BlockHash: 'asasd12',
          TxCount: 4,
          ProposalNodeId: 'dasd',
          Timestamp: 1606669261,
        },
        {
          Id: 2,
          BlockHeight: 102,
          BlockHash: 'asddwerwe',
          TxCount: 3,
          ProposalNodeId: 'ghffw',
          Timestamp: 1606669261,
        },
        {
          Id: 3,
          BlockHeight: 102,
          BlockHash: 'asddwerwe',
          TxCount: 3,
          ProposalNodeId: 'ghffw',
          Timestamp: 1606669261,
        },
        {
          Id: 4,
          BlockHeight: 102,
          BlockHash: 'asddwerwe',
          TxCount: 3,
          ProposalNodeId: 'ghffw',
          Timestamp: 1606669261,
        },
      ],
    },
  },
  GetTxList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          BlockHeight: 1,
          TxId: 'asasd12',
          Sender: 'com',
          SenderOrg: 'org1.com',
          ContractName: 'ChainConfig',
          Status: 0,
          BlockHash: 'dasd',
          Timestamp: 1606669261,
        },
        {
          Id: 2,
          BlockHeight: 1,
          TxId: 'asasd12',
          Sender: 'com',
          SenderOrg: 'org1.com',
          ContractName: 'ChainConfig',
          Status: 0,
          BlockHash: 'dasd',
          Timestamp: 1606669261,
        },
        {
          Id: 3,
          BlockHeight: 1,
          TxId: 'asasd12',
          Sender: 'com',
          SenderOrg: 'org1.com',
          ContractName: 'ChainConfig',
          Status: 0,
          BlockHash: 'dasd',
          Timestamp: 1606669261,
        },
        {
          Id: 4,
          BlockHeight: 1,
          TxId: 'asasd12',
          Sender: 'com',
          SenderOrg: 'org1.com',
          ContractName: 'ChainConfig',
          Status: 1,
          BlockHash: 'dasd',
          Timestamp: 1606669261,
        },
        {
          Id: 5,
          BlockHeight: 1,
          TxId: 'asasd12',
          Sender: 'com',
          SenderOrg: 'org1.com',
          ContractName: 'ChainConfig',
          Status: 1,
          BlockHash: 'dasd',
          Timestamp: 1606669261,
        },
      ],
    },
  },
  GetContractList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          ContractName: 'token',
          CurrentVersion: 'v1.1.0',
          TxCount: 1000,
          Creator: 'a.b.com',
          CreateTimestamp: 1606669263,
          UpgradeUser: 'a.b.com',
          UpgradeTimestamp: 1606669261,
        },
        {
          Id: 2,
          ContractName: 'token',
          CurrentVersion: 'v1.1.0',
          TxCount: 1000,
          Creator: 'a.b.com',
          CreateTimestamp: 1606669263,
          UpgradeUser: 'a.b.com',
          UpgradeTimestamp: 1606669261,
        },
        {
          Id: 3,
          ContractName: 'token',
          CurrentVersion: 'v1.1.0',
          TxCount: 1000,
          Creator: 'a.b.com',
          CreateTimestamp: 1606669263,
          UpgradeUser: 'a.b.com',
          UpgradeTimestamp: 1606669261,
        },
        {
          Id: 4,
          ContractName: 'token',
          CurrentVersion: 'v1.1.0',
          TxCount: 1000,
          Creator: 'a.b.com',
          CreateTimestamp: 1606669263,
          UpgradeUser: 'a.b.com',
          UpgradeTimestamp: 1606669261,
        },
        {
          Id: 5,
          ContractName: 'token',
          CurrentVersion: 'v1.1.0',
          TxCount: 1000,
          Creator: 'a.b.com',
          CreateTimestamp: 1606669263,
          UpgradeUser: 'a.b.com',
          UpgradeTimestamp: 1606669261,
        },
      ],
    },
  },
  GetBlockDetail: {
    Response: {
      Data: {
        BlockHash: 'asdffk2',
        PreBlockHash: 'asdh123123',
        ProposalNodeId: 'common',
        TxRootHash: 'asdkkf',
        TxCount: 2,
        BlockHeight: 17,
        OrgId: 'org1',
        Dag: 'asdkkf',
        Timestamp: 1606669261,
      },
    },
  },
  GetTxDetail: {
    Response: {
      Data: {
        TxId: 'chain1',
        TxHash: 'asdh123123',
        BlockHash: 'asdh123123',
        BlockHeight: 5,
        Sender: 'common',
        ContractName: 'asset',
        ContractVersion: 'v1.0.0',
        TxStatusCode: 'success',
        ContractResultCode: 'OK',
        ContractResult: 'OK',
        RwSetHash: 'fa11a5f1',
        ContractMethod: 'add_evidence',
        ContractParameters:
          '[{"key":"app_id","value":"20051500011"},{"key":"evi_id","value":"5c7100e1d62745d3ab8a91184be3fcca"}]',
        Timestamp: 1606669261,
      },
    },
  },
  Search: {
    Response: {
      Data: {
        Type: 0,
        Id: 4,
        BlockHash: 'ddffww',
      },
    },
  },
  GetContractDetail: {
    Response: {
      Data: {
        ContractName: 'increase',
        CurrentVersion: 'v1.1.0',
        TxCount: 1000,
        Creator: 'a.b.com',
        CreateTxId: 'xxxxxxxxxxx',
        CreateTimestamp: 1606669263,
        UpgradeUser: 'a.b.com',
        UpgradeTimestamp: 1606669261,
      },
    },
  },
  GetEventList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Topic: 'chain1',
          EventInfo: '[xxxxxxxx]',
          Timestamp: 1606669261,
        },
        {
          Topic: 'chain1',
          EventInfo: '[xxxxxxxx]',
          Timestamp: 1606669261,
        },
      ],
    },
  },
  GetChainList: {
    Response: {
      TotalCount: 2,
      GroupList: [
        {
          Id: 1,
          ChainId: 'chain1',
          ChainVersion: 'v2.1.0',
          Consensus: 'PBFT',
          Timestamp: 1606669261,
          Status: 1,
        },
        {
          Id: 2,
          ChainId: 'chain2',
          ChainVersion: 'v2.0.0',
          Consensus: 'PBFT',
          Timestamp: 1606669261,
          Status: 0,
        },
      ],
    },
  },
  SubscribeChain: {
    Response: {
      Data: {
        Id: 2,
        ChainId: 'chain2',
        ChainVersion: 'v2.0.0',
        Consensus: 'PBFT',
        Timestamp: 1606669261,
        Status: 0,
      },
    },
  },
  CancelSubscribe: {
    Response: {
      Data: {
        Id: 2,
        ChainId: 'chain2',
        ChainVersion: 'v2.0.0',
        Consensus: 'PBFT',
        Timestamp: 1606669261,
        Status: 1,
      },
    },
  },
  ModifySubscribe: {
    Response: {
      Data: {
        Id: 2,
        ChainId: 'chain2',
        ChainVersion: 'v2.0.0',
        Consensus: 'PBFT',
        Timestamp: 1606669261,
        Status: 0,
      },
    },
  },
};

export default mock;
