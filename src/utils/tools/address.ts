export const formatAddress = (str: string) => str.replace(/\/ip4\//g, '').replace(/\/tcp\//g, ':');
