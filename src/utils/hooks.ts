import { useSelector } from 'react-redux';
import { useCallback, useMemo, useState } from 'react';
import { RootReducer } from '@src/store';
import { useNavigate } from 'react-router-dom';
import type { NavigateOptions } from 'react-router/dist/lib/context';
import { GetContractCode } from '@utils/apis';
import { GetContractCodeRes } from '@src/models';

/**
 * 获取当前链的chainId
 */
export function useCurrentChainId() {
  const { currentChain } = useSelector((state: RootReducer) => state.chainReducer);
  const chainId = useMemo(() => currentChain?.ChainId ?? '', [currentChain]);
  return chainId;
}

/**
 * 简化链ID下的子路由跳转
 */
export function useNavigateChainChildPath() {
  const chainId = useCurrentChainId();
  const navigate = useNavigate();
  return (path: string, options?: NavigateOptions) => {
    navigate('/' + chainId + path, options);
  };
}

export function useGetContractCode() {
  const [code, setCode] = useState<GetContractCodeRes | null>(null);
  const fetch = useCallback(({
    chainId,
    contractAddr,
    contractVersion,
  }: {
    chainId: string;
    contractAddr: string;
    contractVersion: string;
  }) => {
    GetContractCode({
      ChainId: chainId,
      ContractAddr: contractAddr,
      ContractVersion: contractVersion,
    }).then((res) => {
      if (res.Data) {
        setCode(res.Data);
      }
    });
  }, []);
  return {
    fetch,
    code,
  };
}
