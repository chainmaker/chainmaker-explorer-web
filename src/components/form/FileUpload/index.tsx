import { message, Upload } from 'tea-component';
import style from './index.module.scss';
import React, { useCallback, useState } from 'react';
import classNames from 'classnames';
import { UploadProps } from 'tea-component/lib/upload/Upload';
import { FileError } from 'react-dropzone';

export default function FileUpload({
  value,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onChange,
  accept,
  className,
  useReader = true,
  placeholder,
  noMultiAccept = [],
  sumMaxSize,
  ...others
}: {
  value: any;
  onChange: (v: String | File | File[]) => void;
  accept?: string[] | string;
  className?: string;
  useReader?: boolean;
  placeholder?: string;
  noMultiAccept?: string[]; // 开启多文件上传时，多文件类型存在部分不支持多文件类型时，需要过滤掉
  sumMaxSize?: number;
} & Partial<UploadProps>) {
  const [fileList, setFileList] = useState([]);
  const beforeUpload = useCallback(
    (file: File, fileList: File[], isAccepted, reasons: FileError[]) => {
      if (!isAccepted) {
        message.error({
          content: reasons.map((item) => item.message).join(','),
        });
        onChange('');
        return false;
      }
      if (others.multiple && fileList.length > 1) {
        if (noMultiAccept.some(suffix => file.name.endsWith(suffix))) {
          message.error({
            content: '该格式不支持多文件上传，请上传单个文件。',
          });
          onChange('');
          return false;
        }
        if (sumMaxSize && fileList.reduce((sum, item) => sum + item.size, 0) > sumMaxSize) {
          message.error({
            content: `文件总大小不能超过${sumMaxSize}MB`,
          });
          onChange('');
          return false;
        }
      }
      setFileList(fileList);
      if (!useReader) {
        onChange(others.multiple ? fileList : file);
        return false;
      }
      const render = new FileReader();
      render.onload = function (e) {
        if (e.target?.result) {
          onChange(e.target.result as string);
        } else {
          message.error({
            content: '文件解析错误，请上传正确的文件。',
          });
        }
      };
      render.readAsText(file);
      return false;
    },
    [noMultiAccept, onChange, others.multiple, sumMaxSize, useReader],
  );

  return value
    ? (
      <div className={classNames(style.upload_c, className)}>
        <div className={style.upload_bt}>
          <div className={style.isupload}>{fileList.map(item => item.name).join('，')} 已上传</div>
          <div onClick={() => onChange('')} className={style.delete}></div>
        </div>
      </div>
    )
    : (
      <Upload beforeUpload={beforeUpload} accept={accept} {...others}>
        <div className={classNames(style.upload_c, className)}>
          <div className={style.upload_bt}>
            <div className={style.file}></div>
            {placeholder ?? '文件上传'}
          </div>
        </div>
      </Upload>
    );
}
