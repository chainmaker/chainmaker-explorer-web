import React from 'react';
import { Bubble, Icon, Text } from 'tea-component';

/**
 * 对于系统中的名词增加解释提示
 */
const GlossaryGuide = ({ title, popover, className }: {
  title: React.ReactNode;
  popover: React.ReactNode;
  className?: string
}) => (
  <>
    <Text className={'tea-mr-1n'}>{title}</Text>
    {popover && (
      <Bubble content={<div className={className}>{popover}</div>}>
        <Icon type="info"/>
      </Bubble>
    )}
  </>
);

export default GlossaryGuide;
