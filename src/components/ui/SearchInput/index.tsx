import { ChangeContext } from 'tea-component/lib/form/controlled';
import { Input } from 'tea-component';
import React, { useCallback } from 'react';
import style from './index.module.scss';

export default function SearchInput({
  bt,
  value,
  onChange,
  placeholder,
  onSubmit,
  defaultValue,
}: {
  bt?: string | undefined;
  value?: string;
  onChange?: (value: string, context?: ChangeContext<React.SyntheticEvent<Element, Event>>) => void;
  placeholder?: string;
  onSubmit?: () => void;
  defaultValue?:string;
}) {
  const onKeyPress = useCallback(
    (e) => {
      if (e.which === 13 && onSubmit) {
        onSubmit();
        e.stopPropagation();
        e.preventDefault();
      }
    },
    [onSubmit],
  );
  return (
    <div className={style.c}>
      <div className={style.b}>
        <div className={style.input}>
          <Input
            style={{ height: 30, fontSize: 14, borderRadius: 4 }}
            size="full"
            value={value}
            placeholder={placeholder}
            onChange={onChange}
            onKeyPress={onKeyPress}
            defaultValue={defaultValue}
          />
        </div>
        <div className={style.bt} onClick={onSubmit}>
          {bt || '搜索'}
        </div>
      </div>
    </div>
  );
}
