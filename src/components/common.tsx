import React from 'react';

export function renderBoolean(value: boolean) {
  return value ? '是' : '否';
}

/**
 * 空内容显示展位图
 */
export function EmptyContentPlaceholder({ children, notEmpty }: {
  children?: React.ReactNode;
  notEmpty: any;
}) {
  if (notEmpty) {
    return <>{children}</>;
  }
  return <div className="empty_list"></div>;
}
