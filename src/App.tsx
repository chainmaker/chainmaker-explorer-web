import React, { Suspense, useCallback, useEffect, useRef, useState } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
import Main from './routes/main';

const AddChain = React.lazy(() => import('./routes/AddChain'));
import { AppContext, AppContextValue } from '@components/context';
import {
  ChainMakerPluginAccount,
  getChainMakerPluginAccount,
  removeChainMakerPluginAccount,
  setChainMakerPluginAccount,
} from '@utils/cookies';
import Version from '@src/routes/main/version';
import { GetFrontWebConfig, GetServerConfig } from '@utils/apis';
import { ConfirmModal, ConfirmModalHandle } from '@components/modal';
import { useDispatch, useSelector } from 'react-redux';
import { RootReducer } from '@src/store';
import { loadMonacoEditor } from '@utils/editor';

function App() {
  const [account, setAccount] = useState<ChainMakerPluginAccount | null>(getChainMakerPluginAccount());
  const [appConfig, setAppConfig] = useState<AppContextValue['appConfig']>({
    APP_IS_SHOW_DB_QUERY: true,
    APP_IS_SHOW_TRANSACTION_ADVANCED_SEARCH: false,
  });
  const [serverConfig, setServerConfig] = useState<AppContextValue['serverConfig']>({
    isSupportAccount: false,
  });

  const confirmModalRef = useRef<ConfirmModalHandle>(null);

  const dispatch = useDispatch();

  const modalState = useSelector((state: RootReducer) => state.confirmModalReducer);

  const setAccountCall = useCallback((token: ChainMakerPluginAccount | null, cb?: Function) => {
    setAccount(token);
    if (token) {
      setChainMakerPluginAccount(token);
    } else {
      removeChainMakerPluginAccount();
    }
    cb?.();
  }, []);

  useEffect(() => {
    GetFrontWebConfig().then(res => {
      setAppConfig(res.data);
    });
    GetServerConfig().then(res => {
      setServerConfig(res.data);
    });
    loadMonacoEditor();
  }, []);

  useEffect(() => {
    if (modalState.visible) {
      confirmModalRef.current?.show({
        ...modalState.config,
        onCancel: () => {
          modalState.config.onCancel?.();
          dispatch({
            type: 'show_confirm_modal'
            , payload: false,
          });
        },
      });
    }
  }, [modalState]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ account, setAccount: setAccountCall, appConfig, serverConfig }}>
        <Routes>
          <Route path="addchain" element={<Suspense fallback={null}><AddChain/></Suspense>}/>
          <Route path=":chainId/*" element={<Main/>}/>
          <Route path="/" element={<Navigate to="home"/>}/>
          <Route path="upgrade" element={<Version.UpgradePage/>}/>
        </Routes>
        <Version.Footer/>
        <ConfirmModal ref={confirmModalRef}/>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
