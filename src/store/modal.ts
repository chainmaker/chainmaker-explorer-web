import { ShowConfig } from '@components/modal';

export const confirmModalReducer = (
  state = {
    visible: false,
    config:{

    },
  },
  action: {
    type: string;
    payload?: ShowConfig;
  },
): {
  visible: boolean;
  config: ShowConfig;
} => {
  let newstate = state;
  if (action.type === 'show_confirm_modal') {
    if (action.payload) {
      newstate = {
        ...state,
        visible: true,
        config: action.payload,
      };
    }
  }
  return newstate;
};
