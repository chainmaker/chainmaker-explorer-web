import { ChainItem, ChainListParam } from '@src/models';

export type ChainReducerState = {
  list: ChainItem[] | null;
  currentChain?: ChainItem;
  listParams: ChainListParam;
};

export const chainReducer = (
  state: ChainReducerState = {
    list: null,
    currentChain: undefined,
    listParams: {
      ChainId: '',
      Limit: 10,
      Offset: 0,
    },
  },
  action: {
    type: string;
    payload?: ChainItem[] | ChainItem;
  },
) => {
  if (action.type === 'UPDATE_CHAINS') {
    if (action.payload) {
      return {
        ...state,
        list: action.payload,
      };
    }
  }
  if (action.type === 'UPDATE_CURRENTCHAIN') {
    if (action.payload) {
      return {
        ...state,
        currentChain: action.payload,
      };
    }
  }

  if (action.type === 'UPDATE_CHAINS_LIST_PARAM') {
    if (action.payload) {
      return {
        ...state,
        listParams: {
          ...state.listParams,
          ...action.payload,
        },
      };
    }
  }
  return state;
};
