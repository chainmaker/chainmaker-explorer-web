import { CrossSubChainListParam, GetCrossTxListParam, UiPaging } from '@src/models';
import { GetRegularQueryTaskListParam } from '@src/types';

export interface GetBlockListParamState extends UiPaging {
  value: string;
  nodeId: string;
}

export const blockListParamReducer = (
  state: GetBlockListParamState = {
    value: '',
    pageIndex: 1,
    pageSize: 10,
    nodeId: '',
  },
  action: {
    type: string;
    payload?: GetBlockListParamState;
  },
): GetBlockListParamState => {
  if (action.type === 'UPDATE_BLOCKLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export interface GetTxListParamInfo extends UiPaging {
  value: string;
  sender: string;
  userAddr: string;
  starttime: number | null;
  endtime: number | null;
  txStatus: 0 | 1 | -1;
  contractName: string;
}

export const txListParamReducer = (
  state: GetTxListParamInfo = {
    value: '',
    sender: '',
    userAddr: '',
    starttime: null,// new Date().getTime()-30*24*60*60*1000,
    endtime: null,// new Date().getTime(),
    pageIndex: 1,
    pageSize: 10,
    txStatus: -1,
    contractName: '',
  },
  action: {
    type: string;
    payload?: GetTxListParamInfo;
  },
): GetTxListParamInfo => {
  if (action.type === 'UPDATE_TXLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export interface GetContractListParamInfo extends UiPaging {
  value: string;
  sender: string;
  tabActiveId: 'ALL' | 'FT' | 'NFT' | 'IDA';
}

export const contractListParamReducer = (
  state: GetContractListParamInfo = {
    value: '',
    sender: '',
    pageIndex: 1,
    pageSize: 10,
    tabActiveId: 'ALL',
  },
  action: {
    type: string;
    payload?: GetContractListParamInfo;
  },
): GetContractListParamInfo => {
  if (action.type === 'UPDATE_CONTRACTLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export interface GetUserListParamState extends UiPaging {
  value: string;
  orgId: string;
}

export const userListParamReducer = (
  state: GetUserListParamState = {
    value: '',
    orgId: '',
    pageIndex: 1,
    pageSize: 10,
  },
  action: {
    type: string;
    payload?: GetUserListParamState;
  },
): GetUserListParamState => {
  if (action.type === 'UPDATE_USERLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export interface GetNodeListParamState extends UiPaging {
  nodeId: string;
  nodeName: string;
  orgId: string;
}

export const nodeListParamReducer = (
  state: GetNodeListParamState = {
    nodeId: '',
    nodeName: '',
    orgId: '',
    pageIndex: 1,
    pageSize: 10,
  },
  action: {
    type: string;
    payload?: GetNodeListParamState;
  },
): GetNodeListParamState => {
  if (action.type === 'UPDATE_NODELIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export interface GetOrgListParamState extends UiPaging {
  value: string;
}

export const orgListParamReducer = (
  state: GetOrgListParamState = {
    value: '',
    pageIndex: 1,
    pageSize: 10,
  },
  action: {
    type: string;
    payload?: GetOrgListParamState;
  },
): GetOrgListParamState => {
  if (action.type === 'UPDATE_ORGLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};


export const sqlListParamReducer = (
  state: GetRegularQueryTaskListParam = {
    pageIndex: 1,
    pageSize: 10,
    chainId: '',
    show: 'all',
    title: '',
    userAddr: '',
  },
  action: {
    type: string;
    payload?: GetRegularQueryTaskListParam;
  },
): GetRegularQueryTaskListParam => {
  if (action.type === 'UPDATE_SQLLIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export const crossChainListParamReducer = (
  state: GetCrossTxListParam = {
    ChainId: undefined,
    Limit: 10,
    Offset: 0,
    CrossId: '',
    FromChainName: '',
    ToChainName: '',
    StartTime: 0,
    EndTime: 0,
  },
  action: {
    type: string;
    payload?: GetCrossTxListParam;
  },
): GetCrossTxListParam => {
  if (action.type === 'UPDATE_CROSS_CHAIN_LIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};

export const subChainListParamReducer = (
  state: CrossSubChainListParam = {
    ChainId: undefined,
    Limit: 10,
    Offset: 0,
  },
  action: {
    type: string;
    payload?: CrossSubChainListParam;
  },
): CrossSubChainListParam => {
  if (action.type === 'UPDATE_SUB_CHAIN_LIST_PARAM') {
    if (action.payload) {
      return { ...state, ...action.payload };
    }
  }
  return state;
};
