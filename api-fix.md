# api 自检情况

- 后端对齐人： dongxuliang [2024-01-10 10:30:00]

###### api 文档有些参数 有单复数变化，需要联调中主意

| API | 描述 | 自检状态 | 自检时备注 | 确认 | 确认说明 |  
| --- | --- | --- | --- | --- | --- |
| GetChainConfig | 链配置是否显示 | 不修 | | 不修 | |
| GetChainList | 查看链列表 | | | 修改 | |
| Search | 首页查询 | 未修 | 参数有改动，涉及新增IU交互 | 修改 | Type 选项直接在前端写死即可 |
| Decimal | 首页数据统计 | 不修 | | 不修 | |
| GetTxNumByTime | 按时间段查询交易量 | 未修 | 参数有改动，参数定义未明确 | 不修 | Interval，StartTime、EndTime 浏览器没用到，不传 |
| GetOrgList | 查看组织列表 | 不修 | | 不修 | |
| GetUserList | 获取链上用户列表 | 未修 | 参数有改动，涉及新增IU交互 | 修改 | · UserId 改为 UserIds。<br/>· UserAddrs 浏览器没用到 |
| GetNodeList | 查看节点列表 | 不修 | | 不修 | |
| GetBlockList | 查看区块列表 | 不修 | | 不修 | |
| GetLatestBlockList | 查看最新区块列表 | 不修 | | 不修 | |
| GetTxList | 查看交易列表 | 未修 | 参数有改动，涉及新增IU交互，但是UI未变化，待确认 | 修改 | · 返参新增 ContractMethod 字段，需要显示。<br/> · 传参的UserAddr 改为 UserAddrs，Sender 改为 Senders <br/>· 传参和返参的 TxStatusCode 改为 TxStatus <br/>· ContractAddr 两字段浏览器不用 |
| GetLatestTxList | 查看最新交易列表 | 不修 | | 不修 | |
| GetLatestContractList | 查看最新合约列表 | 不修 | | 不修 | |
| GetContractList | 查看合约列表 | 未修 | 参数有改动，涉及新增IU交互，待确认 | 修改 | · ContractName 改为 ContractKey。<br/>· Status RuntimeType Creators UpgradeAddrs StartTime EndTime 浏览器不用 |
| GetBlockDetail | 查询区块详情 | 不修 | | 不修 | |
| GetTxDetail | 查询交易详情 | 不修 | | 不修 | |
| GetContractDetail | 查询合约详情 | | | 修改 | |
| GetContractCode | 查询合约源码 | 不修 | | 不修 | |
| GetContractVersionList | 获取合约版本交易列表 | 未修 | 新增多个可搜索参数，但是UI未变化，待沟通 | 不修 | ContractAddr Senders RuntimeType Status 浏览器不用 |
| GetEventList | 获取合约事件列表 | | | 修改 | |
| SubscribeChain | 订阅链（绑定链） | 不修 | | 不修 | |
| CancelSubscribe | 取消订阅信息  | 不修 | | 不修 | |
| ModifySubscribe | 修改订阅信息 | 不修 | | 不修 | |
| DeleteSubscribe | 链删除 | 不修 | | 不修 | |
| GetChainId | | | 项目有api定义，但业务代码未使用到 | 已废弃 | |
| GetCurrentChainId | 获取当前链ID | | 前端写死 原因待沟通 | 删掉 | 没有这个api | 
| GetTransferList | 查询流转记录列表 | 不修 | | 不修 | |
| GetNFTDetail | 查询NFT详情 | | 参数变少 | 不修 | API文档补全参数 |
