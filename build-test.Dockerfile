FROM node:18.16.1 as builder

RUN echo " ------------------Web打包 --------------------"

WORKDIR /chainmaker-explorer-web

COPY . /chainmaker-explorer-web

RUN npm install -g npm@7.24.2
RUN npm install
RUN npx update-browserslist-db@latest
RUN npm run build

RUN echo " ------------------Web容器部署启动 --------------------"

FROM nginx:1.26.1
COPY --from=builder /chainmaker-explorer-web/build /usr/share/nginx/html
COPY deploy/nginx/test.conf.d /etc/nginx/conf.d
EXPOSE 80
