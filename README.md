## 开发规范
- scss
  - 存在共享的样式，文件统一放在 src/assets/styles 目录下, 方便多页面共用同一个 class
  - 不存在共享的样式，在文件同级目录下创建一个 scss 文件 (方便知道哪些样式会影响多个页面，哪些只是影响当前页面)
  - 逐步将业务代码中的长行内样式按照上面的规则挪到 scss 文件，短内联可以暂时不管
  - scss 的变量 统一放在 src/assets/styles/variables.scss
  - 全局样式统一放在 src/assets/styles/global.scss
  - 全局重定制tea组件样式统一放在 src/assets/styles/teaCssCover.scss
- js 常量与常量对应的类型声明统一放在 src/constant 目录下
- 逐步将业务代码中原生实现的组件改为 tea 组件


## 分支说明

分支名 dev/public_{version}  对应社区版浏览器 version版本。
分支名 dev/opennet_{version} 对应开发测试网络浏览器 version版本。

## tag标签说明

tag名称 public/v{version}   对应社区版浏览器 version版本。
tag名称 opennet/v{version}   对应开放测试网络浏览器 version版本。

## 出包流程
### 本地出包环境依赖
安装 `docker` 和 `make`
### 本地出包命令
- 在出包之前，先跟后端确认要出包的`docker镜像名:镜像标签`如：*hub-dev.cnbn.org.cn:17754/opennet/explorer_front:latest*
- 执行 make 快捷命令，快捷命令设置在`./Makefile`中
- 执行完毕，产出的镜像为 `hub-dev.cnbn.org.cn:17754/opennet/explorer_front:latest`
- 确认要将镜像导出给到后端，还是上传到指定的 docker hub 仓库中

**tips**: 项目有好几个 dockerfile文件，具体是使用哪个，在 make 命令配置中查看。命令不满足时，可以在`./Makefile`中自定义 新 make 命令 

## 命令列表
### `npm run start`

启动本地开发
浏览器打开 [http://localhost:3000](http://localhost:3000) 看效果

### `npm run test`

执行测试
通过 [running tests](https://facebook.github.io/create-react-app/docs/running-tests) 查看更多信息
### `yarn run build`

构建项目到build目录

## 测试部署地址 
jenkins:  http://jenkins.chainmaker.org.cn/job/ChainMaker-Exporer-Web-2.2.0_Dev_Pipline/（已废弃）

254部署 v2.2
当前项目目录执行
npm run build&scp -r build root@82.157.129.254:/data/soft/explorer-testnet

开放测试网络2.3.0测试部署
npm run build&scp -r ./build root@172.16.12.170:/home/explorer/chainmaker-explorer-front/

181 k8s 部署，目前docker镜像构建时，eslint的standard规则加载又问题，本地验证完可以先删除这个规则再进行
主链镜像
首先执行 
make docker-main-build
或者
docker build -t 192.168.1.181:5000/opennet/explorer_front:latest -f ./build-main.Dockerfile . 
然后执行
make docker-main-push
或者
docker push 192.168.1.181:5000/opennet/explorer_front:latest
最后重启k8s的 explorer-front-formal 服务
kubectl rollout restart deployment/explorer-front-formal

181 测试链镜像
首先执行 
make docker-test-build
或者
docker build -t 192.168.1.181:5000/opennet/explorer-front-testnet:latest -f ./build-test.Dockerfile . 
然后执行
make docker-test-push
或者
docker push 192.168.1.181:5000/opennet/explorer-front-testnet:latest

最后重启k8s的 explorer-front-testnet 服务
kubectl rollout restart deployment/explorer-front-testnet
## 文档列表

产品文档 [https://www.tapd.cn/47654106/prong/stories/view/1147654106001008373?url_cache_key=465ec1eedc257e5407834ab625b177e0&action_entry_type=story_tree_list](https://www.tapd.cn/47654106/prong/stories/view/1147654106001008373?url_cache_key=465ec1eedc257e5407834ab625b177e0&action_entry_type=story_tree_list)
原型 [https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=skw229j468cxa7p](https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=skw229j468cxa7p)
蓝湖ui文档 [https://lanhuapp.com/web/#/item/project/stage?tid=e1f9669d-9325-4730-88a1-77e3430f4981&pid=d89cbf25-a0f7-45bf-90ee-0e4779d2887d](https://lanhuapp.com/web/#/item/project/stage?tid=e1f9669d-9325-4730-88a1-77e3430f4981&pid=d89cbf25-a0f7-45bf-90ee-0e4779d2887d)

## 新版(V2.3.2版本)长安链浏览器文档
- 产品文档 
- 原型 [https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=slo2kqreih6f99e](https://modao.cc/app/211f90b8cf4bcdb65aff1e327c1cd96df5a65742#screen=slo2kqreih6f99e)
- 蓝湖UI文档 [https://lanhuapp.com/web/#/item/project/stage?pid=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&tid=81a8b4ab-1f67-4483-aa1e-8161e96c0268&see=all](https://lanhuapp.com/web/#/item/project/stage?pid=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&tid=81a8b4ab-1f67-4483-aa1e-8161e96c0268&see=all)
- api 文档 [https://console-docs.apipost.cn/preview/e1da5485523dca11/1366faca72d9dc53](https://console-docs.apipost.cn/preview/e1da5485523dca11/1366faca72d9dc53)

## 主子链文档
- 产品文档
- 原型 [https://modao.cc/proto/RSio4M6Ts751ui3x2yY6l7/sharing?view_mode=read_only #区块链浏览器-分享](https://modao.cc/proto/RSio4M6Ts751ui3x2yY6l7/sharing?view_mode=read_only #区块链浏览器-分享)
- 蓝湖UI文档 [https://lanhuapp.com/web/#/item/project/detailDetach?pid=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&project_id=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&image_id=116f5173-6ecb-4399-9b10-025b59b86f09&fromEditor=true](https://lanhuapp.com/web/#/item/project/detailDetach?pid=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&project_id=3639e36e-cfb3-49be-a6b5-0cf3546eaebf&image_id=116f5173-6ecb-4399-9b10-025b59b86f09&fromEditor=true)
- api 文档 [https://console-docs.apipost.cn/preview/e1da5485523dca11/1366faca72d9dc53?target_id=8dff1e47-1fc1-4c9d-8b1f-6806d118a454](https://console-docs.apipost.cn/preview/e1da5485523dca11/1366faca72d9dc53?target_id=8dff1e47-1fc1-4c9d-8b1f-6806d118a454)

## 本地开发whistle 配置

```

# 代码相同链不同
# opennect 版本测试环境主链浏览器 
/192.168.3.170:17763\/(?!chainmaker/?\?)/  proxy://127.0.0.1:3000
# opennect 版本测试环境测试链浏览器
/192.168.3.170:17764\/(?!chainmaker/?\?)/  proxy://127.0.0.1:3000

```


## 目录

    |--  README.md # 服务功能介绍文档
    |--  src  # 项目代码根目录
        |--  assets #静态资源
        |--  components #公共组件资源
            |-- [name] #存放[name]相关公共组件资源
        |-- models #类型定义文件
            |-- [name] #[name]相关类型接口定义文件
        |-- routes #页面ts组件文件
            |-- addChain #首次进入项目时需要添加一个链
            |-- main #已有链的情况下，默认进入链首页
                |-- [page] #[page]相关页面相关组件
        |-- utils #功能模块代码
            |-- apis #接口文件
            |-- validata #校验格式文件
            |-- mock.ts #mock数据
        |-- #App.tsx #网站入口文件
        |-- #Index.tsx #初始化react文件

## 引用顺序图 原则尽量不出现循环引用

```mermaid
graph TD
    功能模块代码 --> 公共组件
    功能模块代码 --> Page组件
    功能模块代码 --> Page相关组件
    Page相关组件 --> Page组件
    公共组件 --> Page相关组件
    公共组件 --> Page组件
```

